﻿using System.Threading;
using System.Threading.Tasks;
using Bunny.AutoMapper;
using Bunny.Business.Interface.Coin;
using Bunny.Data.EF.IRepository.Coin;
using Bunny.Data.EF.Repository.Coin;
using Bunny.DataTable;
using Bunny.DataTable.Models.Request;
using Bunny.DataTable.Models.Response;
using Bunny.DependencyInjection.Attributes;
using Bunny.Middle.Constants;
using Bunny.Model.Coin;

namespace Bunny.Business.Implement.Coin
{
    [PerRequestDependency(ServiceType = typeof(ICoinBusiness))]
    public class CoinBusiness : ICoinBusiness
    {
        private readonly ICoinRepository _coinRepository;

        public CoinBusiness(ICoinRepository coinRepository)
        {
            _coinRepository = coinRepository;
        }
        public Task<DataTableResponseDataModel<CoinDataTableModel>> GetDataTableAsync(DataTableParamModel model, CancellationToken cancellationToken = default)
        {
            var listData = _coinRepository.Get().QueryTo<CoinDataTableModel>();
            var result = listData.GetDataTableResponse(model);
            return Task.FromResult(result);
        }

        public Task Active(int id, CancellationToken cancellationToken = default)
        {
            var entity = _coinRepository.GetSingle(p => p.Id == id);
            entity.IsActive = !entity.IsActive;
            _coinRepository.Update(entity);
            _coinRepository.SaveChanges();

            return Task.CompletedTask;
        }

        public async Task SetDefaultDisableTimeCoin()
        {
            var lstEntity = _coinRepository.Get();
            foreach (var entity in lstEntity)
            {
                entity.MinDisableNotification = Constant.Coin.DefaultTimeDisableCoin;
                _coinRepository.Update(entity);
            }
            await _coinRepository.SaveChangesAsync();
        }

        public async Task ChangeMinDisable(int id, int numberChange)
        {
            var coinEntity = _coinRepository.GetSingle(p => p.Id == id);
            if (coinEntity != null)
            {
                coinEntity.MinDisableNotification = numberChange;
                _coinRepository.Update(coinEntity);
                await _coinRepository.SaveChangesAsync();
            }
        }
    }
}