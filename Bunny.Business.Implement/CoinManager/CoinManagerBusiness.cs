﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Bunny.AutoMapper;
using Bunny.Business.Interface.CoinManager;
using Bunny.Business.Interface.User;
using Bunny.Core.EnumUtils;
using Bunny.Data.EF.Entities;
using Bunny.Data.EF.IRepository.Coin;
using Bunny.Data.EF.IRepository.NotificationCoin;
using Bunny.Data.EF.IRepository.SettingBinance;
using Bunny.DataTable;
using Bunny.DataTable.Models.Request;
using Bunny.DataTable.Models.Response;
using Bunny.DependencyInjection.Attributes;
using Bunny.Middle.AppConfigs;
using Bunny.Middle.Constants;
using Bunny.Middle.User;
using Bunny.Model.Binance;
using Bunny.Model.Coin;
using Bunny.Model.User;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json;
using Serilog;
using Telegram.Bot;

namespace Bunny.Business.Implement.CoinManager
{
    [PerRequestDependency(ServiceType = typeof(ICoinManagerBusiness))]
    public class CoinManagerBusiness : ICoinManagerBusiness
    {
        private readonly INotificationCoinRepository _notificationCoinRepository;
        private readonly ICoinRepository _coinRepository;
        private readonly ISettingBinanceRepository _settingBinanceRepository;

        private readonly ILogger<CoinManagerBusiness> _logger;

        private readonly ILoggerFactory _loggerService;

        private static readonly HttpClient Client = new HttpClient();

        public CoinManagerBusiness(INotificationCoinRepository notificationCoinRepository, ICoinRepository coinRepository
                                    , ISettingBinanceRepository settingBinanceRepository, ILogger<CoinManagerBusiness> logger, ILoggerFactory loggerService)
        {
            _notificationCoinRepository = notificationCoinRepository;
            _coinRepository = coinRepository;
            _settingBinanceRepository = settingBinanceRepository;

            _logger = logger;

            _loggerService = loggerService;
        }
        public Task<DataTableResponseDataModel<NotificationCoinDataTableModel>> GetDataTableAsync(DataTableParamModel model, CancellationToken cancellationToken = default)
        {
            var listData = _notificationCoinRepository.Get().QueryTo<NotificationCoinDataTableModel>();
            var result = listData.GetDataTableResponse(model);
            return Task.FromResult(result);
        }

        public async Task<List<CoinViewModel>> GetListSymbolAsync()
        {
            var result = new List<CoinViewModel>();
            var symbolUrl = AppConfigs.BinanceConfig.getSymbolsUrl;
            var symbolResponse = await Client.GetStringAsync(symbolUrl).ConfigureAwait(false);
            result = JsonConvert.DeserializeObject<List<CoinViewModel>>(symbolResponse);
            var listCoinEntity = Mapper.Map<List<CoinEntity>>(result);
            var coinEntity = _coinRepository.Get().ToList();
            foreach (var coin in listCoinEntity)
            {
                if (coinEntity.Any(p => p.Symbol.Equals(coin.Symbol))) continue;
                coin.MinDisableNotification = Constant.Coin.DefaultTimeDisableCoin;
                _coinRepository.Add(coin);
            }

            //Remove coin not use
            foreach (var coin in coinEntity)
            {
                if (listCoinEntity.Any(p => p.Symbol.Equals(coin.Symbol))) continue;
                var dateTimeNow = DateTime.UtcNow;
                coin.DeletedTime = dateTimeNow;
                coin.LastUpdatedTime = dateTimeNow;
                _coinRepository.Update(coin);
            }
            _coinRepository.SaveChanges();
            return result;
        }

        public async Task SendValueNotificationAsync()
        {
            

            //get coin symbol
            var symbols = _coinRepository.Get(p => p.IsActive
                                                && p.SettingBinanceId != null).ToList();
            foreach (var item in symbols)
            {
                
            }
        }

        public async Task StartJob(int settingId)
        {
            //get coin symbol
            var symbols = _coinRepository.Get(p => p.IsActive
                                                   && p.SettingBinanceId == settingId).ToList();
            foreach (var item in symbols)
            {
                var lastNotificationCoin = await 
                    _notificationCoinRepository.Get(p => p.Symbol.Equals(item.Symbol)).LastOrDefaultAsync();
                if (item.MinDisableNotification != 0 && ((lastNotificationCoin != null &&
                    lastNotificationCoin.CreatedTime.DateTime.AddMinutes(item.MinDisableNotification) < DateTime.Now) || lastNotificationCoin == null) )
                {
                    #region send notification
                    //setting
                    var binanceSetting = _settingBinanceRepository.Get(p => p.Id == settingId).First();
                    var interval = binanceSetting.Interval.GetDisplayName();
                    var limitPrice = binanceSetting.LimitPrice;
                    var desirePricePercent = binanceSetting.DesirePricePercent;
                    var priceZ = binanceSetting.PriceZ;
                    var limitVolume = binanceSetting.LimitVolume;
                    var desireVolumePercent = binanceSetting.DesireVolumePercent;
                    var volumeZ = binanceSetting.VolumeZ;
                    var limitMaxVolume = binanceSetting.LimitMaxVolume;
                    var desireMaxVolPercent = binanceSetting.DesireMaxVolPercent;
                    var maxVolZ = binanceSetting.MaxVolZ;
                    var indexMaxVolumeSetting = binanceSetting.IndexMaxVolume;

                    var binanceSettingDefault = _settingBinanceRepository.Get().First();
                    var tokenBot = binanceSettingDefault.TokenBotTelegram;
                    var idBot = binanceSettingDefault.RoomIdBotTelegram;

                    var maxLimit = 0;
                    if (limitPrice > limitVolume)
                    {
                        maxLimit = (int)limitPrice;
                    }
                    else
                    {
                        maxLimit = (int)limitVolume;
                    }
                    if (maxLimit < limitMaxVolume)
                    {
                        maxLimit = (int)limitMaxVolume;
                    }

                    var candl = await GetCandlestick(item.Symbol, interval, maxLimit).ConfigureAwait(false);
                    if (!candl.PriceVolume.Any()) {
                        continue;
                    };
                    var priceCurrent = candl.PriceVolume[maxLimit - 1].Price;
                    var priceAvg = (decimal)candl.PriceVolume.GetRange(maxLimit - (int)limitPrice, (int)limitPrice - (int)priceZ).Sum(x => x.Price) / ((int)limitPrice - (int)priceZ);

                    //priceCurrent in current price (real time)
                    var percentPrice = priceAvg != 0 ? priceCurrent / priceAvg * 100 : 0;

                    var volumeCurrent = candl.PriceVolume[maxLimit - 1].Volume;
                    var volumeAvg = (decimal)candl.PriceVolume.GetRange(maxLimit - (int)limitVolume, (int)limitVolume - (int)volumeZ).Sum(x => x.Volume) / ((int)limitVolume - (int)volumeZ);
                    var percentVolume = volumeAvg != 0 ? volumeCurrent / volumeAvg * 100 : 0;

                    var maxVol = candl.PriceVolume.GetRange(maxLimit - (int)limitMaxVolume, (int)limitMaxVolume - (int)maxVolZ).OrderByDescending(p => p.Volume).Select(p => p.Volume).ElementAt(indexMaxVolumeSetting - 1);

                    //var maxVol2 = (decimal)candl.PriceVolume.GetRange(maxLimit - (int)limitMaxVolume, (int)limitMaxVolume - (int)maxVolZ).Max(x => x.Volume);
                    var percentMaxVol = maxVol != 0 ? volumeCurrent / maxVol * 100 : 0;

                    //data

                    var data24hrJson = AppConfigs.BinanceConfig.price24hrUrl + "?symbol=" + item.Symbol;
                    var data24hr = await Client.GetStringAsync(data24hrJson).ConfigureAwait(false);
                    var priceStatistics24Hr = ConvertJsonTo24hr(data24hr);


                    if (percentPrice >= (decimal)desirePricePercent
                        || percentVolume >= (decimal)desireVolumePercent
                        || percentMaxVol >= (decimal)desireMaxVolPercent)
                    {
                        var bodyContent = "Xin chào, "
                                          + item.Symbol + " đã đạt mức yêu cầu. " + "\n";
                        var isSentMessage = false;
                        if (binanceSetting.IsCheckPrice && !binanceSetting.IsCheckMaxVolume &&
                            !binanceSetting.IsCheckVolume)
                        {
                            if (percentPrice >= (decimal)desirePricePercent)
                            {

                                bodyContent = await NotificationPrice(bodyContent, limitPrice, desirePricePercent, percentPrice, priceZ, priceAvg,
                                    priceCurrent).ConfigureAwait(false);

                                bodyContent +=
                                    "* Highest price: " + priceStatistics24Hr.highPrice + "\n" +
                                    "* Lowest  price: " + priceStatistics24Hr.lowPrice + "\n" +
                                    "* Quote volume: " + priceStatistics24Hr.quoteVolume + "\n";

                                bodyContent += "* Time: " + DateTime.Now;

                                await SendTelegram(bodyContent, tokenBot, idBot).ConfigureAwait(false);
                                isSentMessage = true;
                            }

                        }
                        else if (!binanceSetting.IsCheckPrice && binanceSetting.IsCheckMaxVolume &&
                                 !binanceSetting.IsCheckVolume)
                        {
                            if (percentMaxVol >= (decimal)desireMaxVolPercent)
                            {
                                //var data24hrJson = AppConfigs.BinanceConfig.price24hrUrl + "?symbol=" + item.Symbol;
                                //var data24hr = await Client.GetStringAsync(data24hrJson).ConfigureAwait(false);
                                //var priceStatistics24Hr = ConvertJsonTo24hr(data24hr);

                                bodyContent = await NotificationVolumeMax(bodyContent, limitMaxVolume, desireMaxVolPercent, percentMaxVol, maxVolZ, maxVol,
                                    volumeCurrent).ConfigureAwait(false);

                                bodyContent +=
                                    "* Highest price: " + priceStatistics24Hr.highPrice + "\n" +
                                    "* Lowest  price: " + priceStatistics24Hr.lowPrice + "\n" +
                                    "* Quote volume: " + priceStatistics24Hr.quoteVolume + "\n";

                                bodyContent += "* Time: " + DateTime.Now;

                                await SendTelegram(bodyContent, tokenBot, idBot).ConfigureAwait(false);
                                isSentMessage = true;

                            }
                        }
                        else if (!binanceSetting.IsCheckPrice && !binanceSetting.IsCheckMaxVolume &&
                                 binanceSetting.IsCheckVolume)
                        {
                            if (percentVolume >= (decimal)desireVolumePercent)
                            {
                                //var data24hrJson = AppConfigs.BinanceConfig.price24hrUrl + "?symbol=" + item.Symbol;
                                //var data24hr = await Client.GetStringAsync(data24hrJson).ConfigureAwait(false);
                                //var priceStatistics24Hr = ConvertJsonTo24hr(data24hr);

                                bodyContent = await NotificationVolume(bodyContent, limitVolume, desireVolumePercent, percentVolume, volumeZ, volumeAvg,
                                    volumeCurrent).ConfigureAwait(false);

                                bodyContent +=
                                    "* Highest price: " + priceStatistics24Hr.highPrice + "\n" +
                                    "* Lowest  price: " + priceStatistics24Hr.lowPrice + "\n" +
                                    "* Quote volume: " + priceStatistics24Hr.quoteVolume + "\n";

                                bodyContent += "* Time: " + DateTime.Now;

                                await SendTelegram(bodyContent, tokenBot, idBot).ConfigureAwait(false);
                                isSentMessage = true;

                            }
                        }
                        else if (binanceSetting.IsCheckPrice && binanceSetting.IsCheckMaxVolume &&
                                 !binanceSetting.IsCheckVolume)
                        {
                            if (percentPrice >= (decimal)desirePricePercent
                                && percentMaxVol >= (decimal)desireMaxVolPercent)
                            {
                                //var data24hrJson = AppConfigs.BinanceConfig.price24hrUrl + "?symbol=" + item.Symbol;
                                //var data24hr = await Client.GetStringAsync(data24hrJson).ConfigureAwait(false);
                                //var priceStatistics24Hr = ConvertJsonTo24hr(data24hr);

                                bodyContent = await NotificationPrice(bodyContent, limitPrice, desirePricePercent, percentPrice, priceZ, priceAvg,
                                    priceCurrent).ConfigureAwait(false);

                                bodyContent = await NotificationVolumeMax(bodyContent, limitMaxVolume, desireMaxVolPercent, percentMaxVol, maxVolZ, maxVol,
                                    volumeCurrent).ConfigureAwait(false);

                                bodyContent +=
                                    "* Highest price: " + priceStatistics24Hr.highPrice + "\n" +
                                    "* Lowest  price: " + priceStatistics24Hr.lowPrice + "\n" +
                                    "* Quote volume: " + priceStatistics24Hr.quoteVolume + "\n";

                                bodyContent += "* Time: " + DateTime.Now;

                                await SendTelegram(bodyContent, tokenBot, idBot).ConfigureAwait(false);
                                isSentMessage = true;

                            }
                        }
                        else if (binanceSetting.IsCheckPrice && !binanceSetting.IsCheckMaxVolume &&
                                 binanceSetting.IsCheckVolume)
                        {
                            if (percentPrice >= (decimal)desirePricePercent
                                && percentVolume >= (decimal)desireVolumePercent)
                            {
                                //var data24hrJson = AppConfigs.BinanceConfig.price24hrUrl + "?symbol=" + item.Symbol;
                                //var data24hr = await Client.GetStringAsync(data24hrJson).ConfigureAwait(false);
                                //var priceStatistics24Hr = ConvertJsonTo24hr(data24hr);

                                bodyContent = await NotificationPrice(bodyContent, limitPrice, desirePricePercent, percentPrice, priceZ, priceAvg,
                                    priceCurrent).ConfigureAwait(false);

                                bodyContent = await NotificationVolume(bodyContent, limitVolume, desireVolumePercent, percentVolume, volumeZ, volumeAvg,
                                    volumeCurrent).ConfigureAwait(false);

                                bodyContent +=
                                    "* Highest price: " + priceStatistics24Hr.highPrice + "\n" +
                                    "* Lowest  price: " + priceStatistics24Hr.lowPrice + "\n" +
                                    "* Quote volume: " + priceStatistics24Hr.quoteVolume + "\n";

                                bodyContent += "* Time: " + DateTime.Now;

                                await SendTelegram(bodyContent, tokenBot, idBot).ConfigureAwait(false);
                                isSentMessage = true;

                            }
                        }
                        else if (!binanceSetting.IsCheckPrice && binanceSetting.IsCheckMaxVolume &&
                                 binanceSetting.IsCheckVolume)
                        {
                            if (percentVolume >= (decimal)desireVolumePercent
                                && percentMaxVol >= (decimal)desireMaxVolPercent)
                            {
                                //var data24hrJson = AppConfigs.BinanceConfig.price24hrUrl + "?symbol=" + item.Symbol;
                                //var data24hr = await Client.GetStringAsync(data24hrJson).ConfigureAwait(false);
                                //var priceStatistics24Hr = ConvertJsonTo24hr(data24hr);

                                bodyContent = await NotificationVolume(bodyContent, limitVolume, desireVolumePercent, percentVolume, volumeZ, volumeAvg,
                                    volumeCurrent).ConfigureAwait(false);

                                bodyContent = await NotificationVolumeMax(bodyContent, limitMaxVolume, desireMaxVolPercent, percentMaxVol, maxVolZ, maxVol,
                                    volumeCurrent).ConfigureAwait(false);

                                bodyContent +=
                                    "* Highest price: " + priceStatistics24Hr.highPrice + "\n" +
                                    "* Lowest  price: " + priceStatistics24Hr.lowPrice + "\n" +
                                    "* Quote volume: " + priceStatistics24Hr.quoteVolume + "\n";

                                bodyContent += "* Time: " + DateTime.Now;

                                await SendTelegram(bodyContent, tokenBot, idBot).ConfigureAwait(false);
                                isSentMessage = true;

                            }
                        }
                        else if (binanceSetting.IsCheckPrice && binanceSetting.IsCheckMaxVolume &&
                                 binanceSetting.IsCheckVolume)
                        {
                            if (percentPrice >= (decimal)desirePricePercent
                                && percentVolume >= (decimal)desireVolumePercent
                                && percentMaxVol >= (decimal)desireMaxVolPercent)
                            {
                                //var data24hrJson = AppConfigs.BinanceConfig.price24hrUrl + "?symbol=" + item.Symbol;
                                //var data24hr = await Client.GetStringAsync(data24hrJson).ConfigureAwait(false);
                                //var priceStatistics24Hr = ConvertJsonTo24hr(data24hr);

                                bodyContent = await NotificationPrice(bodyContent, limitPrice, desirePricePercent, percentPrice, priceZ, priceAvg,
                                    priceCurrent).ConfigureAwait(false);

                                bodyContent = await NotificationVolume(bodyContent, limitVolume, desireVolumePercent, percentVolume, volumeZ, volumeAvg,
                                    volumeCurrent).ConfigureAwait(false);

                                bodyContent = await NotificationVolumeMax(bodyContent, limitMaxVolume, desireMaxVolPercent, percentMaxVol, maxVolZ, maxVol,
                                    volumeCurrent).ConfigureAwait(false);

                                bodyContent +=
                                    "* Highest price: " + priceStatistics24Hr.highPrice + "\n" +
                                    "* Lowest  price: " + priceStatistics24Hr.lowPrice + "\n" +
                                    "* Quote volume: " + priceStatistics24Hr.quoteVolume + "\n";

                                bodyContent += "* Time: " + DateTime.Now;

                                await SendTelegram(bodyContent, tokenBot, idBot).ConfigureAwait(false);
                                isSentMessage = true;

                            }
                        }

                        if (isSentMessage)
                        {
                            _notificationCoinRepository.Add(new NotificationCoinEntity
                            {
                                Symbol = item.Symbol,
                                AvgValuePrice = priceAvg,
                                AvgValueVolume = 0,
                                AvgValueVolumeMax = 0,
                                CurrentValuePrice = priceCurrent,
                                CurrentValueVolume = volumeCurrent,
                                CurrentValueVolumeMax = maxVol,
                                HighestPrice = priceStatistics24Hr.highPrice,
                                IncreasePercentPrice = percentPrice,
                                IncreasePercentVolume = 0,
                                IncreasePercentVolumeMax = 0,
                                LowestPrice = priceStatistics24Hr.lowPrice,
                                QuoteVolume = priceStatistics24Hr.quoteVolume,
                                Name = item.Symbol,
                                TimeNotification = DateTime.Now,
                                UserId = 1
                            });
                            await _notificationCoinRepository.SaveChangesAsync();
                        }
                       
                    }
                    #endregion send notification
                }

            }

            //await SendTelegramForMeToTest().ConfigureAwait(false);
        }

        #region private class

        private Task<string> NotificationPrice(string bodyContent, double limitPrice, double desirePricePercent, decimal percentPrice
                                                , double priceZ, decimal priceAvg, decimal priceCurrent)
        {
            if (desirePricePercent != int.MinValue)
            {
                bodyContent += "Price: " + "\n" +
                               "- Giá trị X: " + limitPrice + "" + "\n" +
                               "- Giá trị Y: " + desirePricePercent + "% (tăng " + Math.Round(percentPrice - (decimal)desirePricePercent, 2) + "%)" + "\n" +
                               "- Giá trị Z: " + priceZ + "\n" +
                               "- Price trung bình: " + Math.Round(priceAvg, 8) + " " + "\n" +
                               "- Price hiện tại: " + priceCurrent + "\n";
            }
                
            return Task.FromResult(bodyContent);
        }

        private Task<string> NotificationVolume(string bodyContent, double limitVolume, double desireVolumePercent, decimal percentVolume
                                                , double volumeZ, decimal volumeAvg, decimal volumeCurrent)
        {
            if (desireVolumePercent != int.MinValue)
            {
                bodyContent += "Volume: " + "\n" +
                               "- Giá trị X: " + limitVolume + "\n" +
                               "- Giá trị Y: " + desireVolumePercent + "% (tăng " + Math.Round(percentVolume - (decimal)desireVolumePercent, 2).ToString() + "%)" + "\n" +
                               "- Giá trị Z: " + volumeZ.ToString() + "\n" +
                               "- Volume trung bình: " + Math.Round(volumeAvg, 8) + "\n" +
                               "- Volume hiện tại: " + volumeCurrent + "\n";
            }

            return Task.FromResult(bodyContent);
        }

        private Task<string> NotificationVolumeMax(string bodyContent, double limitMaxVolume, double desireMaxVolPercent, decimal percentMaxVol
            , double maxVolZ, decimal maxVol, decimal volumeCurrent)
        {
            if (desireMaxVolPercent != int.MinValue)
            {
                bodyContent += "Max volume: " + "\n" +
                               "- Giá trị X: " + limitMaxVolume + "\n" +
                               "- Giá trị Y: " + desireMaxVolPercent + "% (tăng " + Math.Round(percentMaxVol - (decimal)desireMaxVolPercent, 2) + "%)" +
                               "- Giá trị Z: " + maxVolZ + "\n" +
                               "- Max Volume: " + maxVol + "\n" +
                               "- Volume hiện tại: " + volumeCurrent + "\n";
            }

            return Task.FromResult(bodyContent);
        }

        private async Task SendTelegram(string body, string tokenBot, string idBot)
        {
            //_logger.LogInformation("send messager successfully");
            //await WriteLogTest();
            var bot = new TelegramBotClient(tokenBot);
            var s = await bot.SendTextMessageAsync(idBot, body);
        }

        public async Task WriteLogTest()
        {
            // Create logger
            var logger = _loggerService.CreateLogger<CoinManagerBusiness>();
            // run app logic
            logger.LogInformation("Hello! This is log message from .NET console app...");

            logger.LogDebug("Starting web host 123");

            Log.Information("Starting web host");
        }

        private async Task SendTelegramForMeToTest()
        {
            var bot = new TelegramBotClient("790784211:AAF-ylli3j4ZAWJMyT8BMwCO_V9iY9Rs5pk");
            var s = await bot.SendTextMessageAsync("692898818", "Job start: " + DateTime.UtcNow + " - Localhost test");
        }

        private async Task<Candlestick> GetCandlestick(string symbol, string interval, int limit)
        {
            try
            {
                var klinesApiUrl = AppConfigs.BinanceConfig.klinesApiUrl;
                var api = string.Format(klinesApiUrl + "?symbol={0}&interval={1}&limit={2}", symbol, interval, limit);
                var strData = await Client.GetStringAsync(api).ConfigureAwait(false);
                var result = ConvertJsonToCandlestick(strData);
                result.Symbol = symbol;
                return result;
            }
            catch (Exception ex)
            {
                return new Candlestick();
            }
        }

        private Candlestick ConvertJsonToCandlestick(string jsonString)
        {
            try
            {
                var objs = JsonConvert.DeserializeObject<dynamic>(jsonString);
                var priceVolumes = new List<PriceVolume>();
                foreach (var item in objs)
                {
                    //priceVolumes.Add(new PriceVolume(decimal.Parse(item.Five), decimal.Parse(item.Six)));
                    //var res = item as object[];
                    if (item != null)
                    {
                        var a = decimal.Parse(item[4].ToString());
                        var b = decimal.Parse(item[5].ToString());
                        //string[] sRes = res.OfType<string>().ToArray();
                        priceVolumes.Add(new PriceVolume(a, b));
                    }
                }

                var result = new Candlestick();
                result.PriceVolume = priceVolumes;
                return result;
            }
            catch (Exception ex)
            {
                return new Candlestick();
            }
        }

        private PriceStatistics24hr ConvertJsonTo24hr(string jsonString)
        {
            try
            {
                var result = JsonConvert.DeserializeObject<PriceStatistics24hr>(jsonString);
                return result;
            }
            catch (Exception ex)
            {
                return new PriceStatistics24hr();
            }
        }
        #endregion private class
    }
}