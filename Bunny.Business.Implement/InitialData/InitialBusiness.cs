﻿using System;
using System.Linq;
using Bunny.Business.Interface.InitialData;
using Bunny.Core.EnumUtils;
using Bunny.Core.StringUtils;
using Bunny.Data.EF.Entities;
using Bunny.Data.EF.IRepository.Role;
using Bunny.Data.EF.IRepository.SettingBinance;
using Bunny.Data.EF.IRepository.User;
using Bunny.DependencyInjection.Attributes;
using Bunny.Middle;
using Bunny.Middle.AppConfigs;
using Microsoft.EntityFrameworkCore.Internal;

namespace Bunny.Business.Implement.InitialData
{
    [PerRequestDependency(ServiceType = typeof(IInitialBusiness))]
    public class InitialBusiness : IInitialBusiness
    {
        public readonly IUserRepository _userRepository;
        public readonly IRoleRepository _roleRepository;
        public readonly ISettingBinanceRepository _settingBinanceRepository;

        public InitialBusiness(IUserRepository userRepository, IRoleRepository roleRepository, ISettingBinanceRepository settingBinanceRepository)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _settingBinanceRepository = settingBinanceRepository;
        }
        public void DummyData()
        {
            DummyRole();
            DummyUser();
            DummySettingBinance();
        }

        #region private class

        private void DummyRole()
        {
            var listRole = EnumHelper.GetListEnum<Enums.Role>();
            foreach (var role in listRole)
            {
                var m = role;
                var newGuid = Guid.NewGuid();
                if (!EnumerableExtensions.Any(_roleRepository.Get(p => p.Type == role)))
                {
                    _roleRepository.Add(new RoleEntity
                    {
                        GlobalId = newGuid.ToString(),
                        Type = role,
                        Name = role.GetDisplayName(),
                        Description = role.GetDescription(),
                    });
                    _roleRepository.SaveChanges();
                }
            }
        }
        private void DummyUser()
        {
            if (!EnumerableExtensions.Any(_userRepository.Get()))
            {
                //Get admin role
                var role = _roleRepository.GetSingle(p => p.Type == Enums.Role.Admin);
                //Add user
                var key = AppConfigs.DataHashConfig.Key;
                var newGuid = Guid.NewGuid();
                _userRepository.Add(new UserEntity
                {
                    GlobalId = newGuid.ToString(),
                    Subject = newGuid.ToString(),
                    UserName = BusinessConstant.DummyUser.DefaultUserName,
                    Password = BusinessConstant.DummyUser.DefaultPassword.Encrypt(key),
                    LastName = "Vu",
                    FirstName = "Le",
                    PhoneNumber = "0857488989",
                    RoleId = role?.Id ?? 0,
                    IsActive = true
                });
                _userRepository.SaveChanges();
            }
            
        }
        private void DummySettingBinance()
        {
            if (!EnumerableExtensions.Any(_settingBinanceRepository.Get()))
            {
                var newGuid = Guid.NewGuid();
                _settingBinanceRepository.Add(new SettingBinanceEntity
                {
                    GlobalId = newGuid.ToString(),
                    LimitPrice = 0,
                    DesirePricePercent = 0,
                    PriceZ = 0,

                    LimitVolume = 0,
                    DesireVolumePercent = 0,
                    VolumeZ = 0,

                    LimitMaxVolume = 0,
                    DesireMaxVolPercent = 0,
                    MaxVolZ = 0,

                    IndexMaxVolume = 1,
                    TokenBotTelegram = "790784211:AAF-ylli3j4ZAWJMyT8BMwCO_V9iY9Rs5pk",
                    RoomIdBotTelegram = "692898818"
                });
                _settingBinanceRepository.SaveChanges();
            }

            if (_settingBinanceRepository.Get().Count() == 1)
            {
                var newGuid = Guid.NewGuid();
                _settingBinanceRepository.Add(new SettingBinanceEntity
                {
                    GlobalId = newGuid.ToString(),
                    LimitPrice = 0,
                    DesirePricePercent = 0,
                    PriceZ = 0,

                    LimitVolume = 0,
                    DesireVolumePercent = 0,
                    VolumeZ = 0,

                    LimitMaxVolume = 0,
                    DesireMaxVolPercent = 0,
                    MaxVolZ = 0,

                    IndexMaxVolume = 1,
                    TokenBotTelegram = "790784211:AAF-ylli3j4ZAWJMyT8BMwCO_V9iY9Rs5pk",
                    RoomIdBotTelegram = "692898818"
                });

                var newGuid2 = Guid.NewGuid();
                _settingBinanceRepository.Add(new SettingBinanceEntity
                {
                    GlobalId = newGuid2.ToString(),
                    LimitPrice = 0,
                    DesirePricePercent = 0,
                    PriceZ = 0,

                    LimitVolume = 0,
                    DesireVolumePercent = 0,
                    VolumeZ = 0,

                    LimitMaxVolume = 0,
                    DesireMaxVolPercent = 0,
                    MaxVolZ = 0,

                    IndexMaxVolume = 1,
                    TokenBotTelegram = "790784211:AAF-ylli3j4ZAWJMyT8BMwCO_V9iY9Rs5pk",
                    RoomIdBotTelegram = "692898818"
                });
                _settingBinanceRepository.SaveChanges();

            }
        }
        #endregion private class
    }
}