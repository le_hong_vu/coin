﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Bunny.AutoMapper;
using Bunny.Business.Interface.MasterPage;
using Bunny.Core.HttpUtils;
using Bunny.Core.HttpUtils.HttpDetection.Device;
using Bunny.Core.StringUtils;
using Bunny.Data.EF.Entities;
using Bunny.Data.EF.IRepository.RefreshToken;
using Bunny.Data.EF.IRepository.User;
using Bunny.DependencyInjection.Attributes;
using Bunny.Middle;
using Bunny.Middle.AppConfigs;
using Bunny.Middle.Exceptions;
using Bunny.Middle.Model.User;

namespace Bunny.Business.Implement.MasterPage
{
    [PerRequestDependency(ServiceType = typeof(IMasterPageBusiness))]
    public class MasterPageBusiness : IMasterPageBusiness
    {
        private readonly IUserRepository _userRepository;
        private readonly IRefreshTokenRepository _refreshTokenRepository;

        public MasterPageBusiness(IUserRepository userRepository, IRefreshTokenRepository refreshTokenRepository)
        {
            _userRepository = userRepository;
            _refreshTokenRepository = refreshTokenRepository;
        }
        public LoggedInUserModel SignIn(string userName, out string refreshToken, int? clientId)
        {
            userName = StringHelper.Normalize(userName);

            var user = _userRepository.Get(x => x.UserName == userName).Single();

            // Get logged in user
            var loggedInUserModel = user.MapTo<LoggedInUserModel>();
            // Generate and save refresh token
            refreshToken = GenerateRefreshToken(loggedInUserModel.Id);

            return loggedInUserModel;
        }

        public Task<bool> CheckValidSignIn(string userName, string password)
        {
            var key = AppConfigs.DataHashConfig.Key;
            userName = StringHelper.Normalize(userName);
            password = password.Encrypt(key);
            var user = _userRepository.Get(p => p.UserName == userName && p.Password == password && p.IsActive);
            if (!user.Any())
            {
                throw new BunnyException(ErrorCode.UserPasswordOrUserNameWrong);
            }

            return Task.FromResult(true);
        }

        public void CheckValidRefreshToken(string refreshToken, int? clientId)
        {
            var systemNow = SystemUtils.SystemTimeNow;

            var isValidRefreshToken = _refreshTokenRepository.Get().Any(x => x.RefreshToken == refreshToken && (x.ExpireOn == null || systemNow < x.ExpireOn));

            if (!isValidRefreshToken)
                throw new BunnyException(ErrorCode.InvalidRefreshToken);
        }

        public Task<LoggedInUserModel> GetLoggedInUserByRefreshTokenAsync(string refreshToken, CancellationToken cancellationToken = default)
        {
            var refreshTokenEntity = _refreshTokenRepository.Get(x => x.RefreshToken == refreshToken).Select(x => new RefreshTokenEntity
            {
                Id = x.Id,
                TotalUsage = x.TotalUsage,
                UserId = x.UserId
            }).Single();

            var loggedInUser = _userRepository.Get(x => x.Id == refreshTokenEntity.UserId).QueryTo<LoggedInUserModel>().Single();
            //loggedInUser.EmployeeId = _employeeRepository.Get(x => x.UserId == loggedInUser.Id).Select(x => x.Id).FirstOrDefault();
            //loggedInUser.ManageCompanyId = _companyRepository.Get(x => x.PersonInChargeUserId == loggedInUser.Id).Select(x => x.Id).FirstOrDefault();
            // Increase total usage
            refreshTokenEntity.TotalUsage++;

            _refreshTokenRepository.Update(refreshTokenEntity, x => x.TotalUsage);

            // Check cancellation token
            cancellationToken.ThrowIfCancellationRequested();

            _refreshTokenRepository.SaveChanges();

            return Task.FromResult(loggedInUser);
        }

        public Task<LoggedInUserModel> GetLoggedInUserBySubjectAsync(string subject, CancellationToken cancellationToken = default)
        {
            var loggedInUser = _userRepository.Get(x => x.GlobalId == subject).QueryTo<LoggedInUserModel>().Single();
            return Task.FromResult(loggedInUser);
        }

        #region private class

        private string GenerateRefreshToken(int userId)
        {
            var refreshToken = Guid.NewGuid().ToString();

            var deviceInfo = HttpContext.Current?.Request.GetDeviceInfo();

            var refreshTokenEntity = new RefreshTokenEntity
            {
                RefreshToken = refreshToken,
                ExpireOn = null,
                UserId = userId,
                TotalUsage = 1,
                DeviceType = deviceInfo?.Type ?? DeviceType.Unknown,
                MarkerName = deviceInfo?.MarkerName,
                MarkerVersion = deviceInfo?.MarkerVersion,
                OsName = deviceInfo?.OsName,
                OsVersion = deviceInfo?.OsVersion,
                EngineName = deviceInfo?.EngineName,
                EngineVersion = deviceInfo?.EngineVersion,
                BrowserName = deviceInfo?.BrowserName,
                BrowserVersion = deviceInfo?.BrowserVersion,
                IpAddress = deviceInfo?.IpAddress,
                CityName = deviceInfo?.CityName,
                CityGeoNameId = deviceInfo?.CityGeoNameId,
                CountryName = deviceInfo?.CountryName,
                CountryGeoNameId = deviceInfo?.CountryGeoNameId,
                CountryIsoCode = deviceInfo?.CountryIsoCode,
                ContinentName = deviceInfo?.ContinentName,
                ContinentGeoNameId = deviceInfo?.ContinentGeoNameId,
                ContinentCode = deviceInfo?.ContinentCode,
                TimeZone = deviceInfo?.TimeZone,
                Latitude = deviceInfo?.Latitude,
                Longitude = deviceInfo?.Longitude,
                AccuracyRadius = deviceInfo?.AccuracyRadius,
                PostalCode = deviceInfo?.PostalCode,
                UserAgent = deviceInfo?.UserAgent,
                DeviceHash = deviceInfo?.DeviceHash
            };

            _refreshTokenRepository.Add(refreshTokenEntity);

            _refreshTokenRepository.SaveChanges();

            return refreshToken;
        }

        #endregion private class
    }
}