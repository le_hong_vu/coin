﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using AutoMapper;
using Bunny.Business.Interface.SettingBinance;
using Bunny.Core.EnumUtils;
using Bunny.Data.EF.Entities;
using Bunny.Data.EF.IRepository.Coin;
using Bunny.Data.EF.IRepository.SettingBinance;
using Bunny.DependencyInjection.Attributes;
using Bunny.Model;
using Bunny.Model.SettingBinance;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace Bunny.Business.Implement.SettingBinance
{
    [PerRequestDependency(ServiceType = typeof(ISettingBinanceBusiness))]
    public class SettingBinanceBusiness : ISettingBinanceBusiness
    {
        private readonly ISettingBinanceRepository _settingBinanceRepository;
        private readonly ICoinRepository _coinRepository;
        public SettingBinanceBusiness(ISettingBinanceRepository settingBinanceRepository, ICoinRepository coinRepository)
        {
            _settingBinanceRepository = settingBinanceRepository;
            _coinRepository = coinRepository;
        }
        public Task<List<SettingBinanceEditModel>> GetSettingBinance()
        {
            var entity = _settingBinanceRepository.Get().Include(p => p.Coin).ToList();
            var result = Mapper.Map<List<SettingBinanceEditModel>>(entity);
            foreach (var item in result)
            {
                item.ListIntervalSetting = EnumHelper.GetListEnum<EnumsApp.IntervalSetting>().Select(x => new SelectListItem
                {
                    Value = x.ToString(),
                    Text = x.GetDisplayName(),
                }).ToList();

                item.ListCoin = _coinRepository.Get(p => (p.SettingBinanceId == item.Id || p.SettingBinanceId == null) && p.IsActive).Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Symbol,
                }).ToList();

                item.ListCoinId = item.Coin.Select(p => p.Id).ToArray();
            }
            return Task.FromResult(result);
        }

        public Task<SettingBinanceEditModel> EditSettingBinance(SettingBinanceEditModel model, int userCurrentId)
        {
            var entity = _settingBinanceRepository.Get(p => p.Id == model.Id).First();
            entity.LimitPrice = model.LimitPrice;
            entity.DesirePricePercent = model.DesirePricePercent;
            entity.PriceZ = model.PriceZ;

            entity.LimitVolume = model.LimitVolume;
            entity.DesireVolumePercent = model.DesireVolumePercent;
            entity.VolumeZ = model.VolumeZ;

            entity.LimitMaxVolume = model.LimitMaxVolume;
            entity.DesireMaxVolPercent = model.DesireMaxVolPercent;
            entity.MaxVolZ = model.MaxVolZ;

            entity.Interval = model.Interval;

            entity.IsCheckVolume = model.IsCheckVolume;
            entity.IsCheckMaxVolume = model.IsCheckMaxVolume;
            entity.IsCheckPrice = model.IsCheckPrice;

            entity.TokenBotTelegram = model.TokenBotTelegram;
            entity.RoomIdBotTelegram = model.RoomIdBotTelegram;

            entity.IndexMaxVolume = model.IndexMaxVolume;

            entity.SafePercentForBuy = model.SafePercentForBuy;
            entity.TotalColumnForBuy = model.TotalColumnForBuy;
            entity.SafePercentForSell = model.SafePercentForSell;
            entity.TotalColumnForSell = model.TotalColumnForSell;

            entity.LastUpdatedBy = userCurrentId;
            entity.LastUpdatedTime = DateTime.UtcNow;

            if (model.ListCoinId != null && model.ListCoinId.Any())
            {
                RemoveListCoinOld(model.Id);
                foreach (var result in model.ListCoinId)
                {
                    var coinEntity = _coinRepository.Get(p => p.Id == result).First();
                    coinEntity.SettingBinanceId = model.Id;
                    coinEntity.LastUpdatedTime = DateTime.UtcNow;
                    coinEntity.LastUpdatedBy = userCurrentId;

                    _coinRepository.Update(coinEntity);
                }

                _coinRepository.SaveChanges();
            }

            _settingBinanceRepository.Update(entity);
            _settingBinanceRepository.SaveChanges();

            return Task.FromResult(Mapper.Map<SettingBinanceEditModel>(entity));
        }

        public Task<List<SettingBinanceEntity>> GetAllSettingBinanceEntity()
        {
            return Task.FromResult(_settingBinanceRepository.Get().ToList());
        }

        private void RemoveListCoinOld(int settingId)
        {
            var coinOfSetting = _coinRepository.Get(p => p.SettingBinanceId == settingId).ToList();
            foreach (var item in coinOfSetting)
            {
                item.SettingBinanceId = null;
                _coinRepository.Update(item);
            }

            _coinRepository.SaveChanges();
        }
    }
}