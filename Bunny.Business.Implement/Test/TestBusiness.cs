﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Bunny.AutoMapper;
using Bunny.Business.Interface.Test;
using Bunny.Data.EF.Entities;
using Bunny.Data.EF.IRepository.User;
using Bunny.DependencyInjection.Attributes;
using Bunny.Model.User;
using Microsoft.EntityFrameworkCore;

namespace Bunny.Business.Implement.Test
{
    [PerRequestDependency(ServiceType = typeof(ITestBusiness))]
    public class TestBusiness : ITestBusiness
    {
        private readonly IUserRepository _userRepository;

        public TestBusiness(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public Task<UserEntity> TestAddEntity(CancellationToken cancellationToken = default)
        {
            var globalId = Guid.NewGuid().ToString();
            var userEntity = new UserEntity
            {
                CreatedTime = DateTimeOffset.UtcNow,
                FirstName = "Vu" + Guid.NewGuid(),
                LastName = "Le" + Guid.NewGuid(),
                GlobalId = globalId,
                Password = "lehongvu",
                UserName = "VuLe" + Guid.NewGuid(),
                PhoneNumber = "0857488989",
                IsActive = true
            };
            _userRepository.Add(userEntity);
            _userRepository.SaveChangesAsync(cancellationToken);
            var result = _userRepository.GetSingle(p => p.GlobalId.Contains(globalId));
            return Task.FromResult(result);
        }

        public Task<List<UserViewModel>> GetAllUserEntity(CancellationToken cancellationToken = default)
        {
            var userEntity = _userRepository.Get().Include(p => p.Role).ToList();
            var userViewModel = userEntity.MapTo<List<UserViewModel>>();
            return Task.FromResult(userViewModel);
        }
    }
}