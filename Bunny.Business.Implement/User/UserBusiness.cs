﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Bunny.AutoMapper;
using Bunny.Business.Interface.User;
using Bunny.Core.StringUtils;
using Bunny.Data.EF.Entities;
using Bunny.Data.EF.IRepository.Role;
using Bunny.Data.EF.IRepository.User;
using Bunny.DataTable;
using Bunny.DataTable.Models.Request;
using Bunny.DataTable.Models.Response;
using Bunny.DependencyInjection.Attributes;
using Bunny.Middle.AppConfigs;
using Bunny.Middle.Exceptions;
using Bunny.Model.User;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Bunny.Business.Implement.User
{
    [PerRequestDependency(ServiceType = typeof(IUserBusiness))]
    public class UserBusiness : IUserBusiness
    {
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;

        public UserBusiness(IUserRepository userRepository, IRoleRepository roleRepository)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }

        public Task<DataTableResponseDataModel<UserDataTableModel>> GetDataTableAsync(DataTableParamModel model, CancellationToken cancellationToken = default)
        {
            var listData = _userRepository.Get().QueryTo<UserDataTableModel>();
            var result = listData.GetDataTableResponse(model);
            return Task.FromResult(result);
        }

        public Task<List<UserViewModel>> GetAllUser()
        {
            var result = Mapper.Map<List<UserViewModel>>(_userRepository.Get().ToList());
            return Task.FromResult(result);
        }

        public Task<UserEditModel> GetEdit(int id, CancellationToken cancellationToken = default)
        {
            var entity = _userRepository.GetSingle(p => p.Id == id);
            var result = Mapper.Map<UserEditModel>(entity);
            result.ListRole = _roleRepository.Get().Select(p => new SelectListItem
            {
                Text = p.Name,
                Value = p.Id.ToString()
            }).ToList();
            return Task.FromResult(result);
        }

        public Task<UserAddModel> GetAdd(CancellationToken cancellationToken = default)
        {
            var result = new UserAddModel();
            result.ListRole = _roleRepository.Get().Select(p => new SelectListItem
            {
                Text = p.Name,
                Value = p.Id.ToString()
            }).ToList();

            return Task.FromResult(result);
        }

        public Task<int> SubmitAdd(UserAddModel model, int userId, CancellationToken cancellationToken = default)
        {
            if (model.RoleId == 0) throw new BunnyException(ErrorCode.RoleRequired);
            var key = AppConfigs.DataHashConfig.Key;
            var globalId = Guid.NewGuid();
            var result = new UserEntity
            {
                CreatedBy = userId,
                CreatedTime = DateTime.UtcNow,
                FirstName = model.FirstName,
                LastName = model.LastName,
                IsActive = true,
                Password = model.Password.Encrypt(key),
                RoleId = model.RoleId,
                UserName = model.UserName,
                PhoneNumber = model.PhoneNumber,
                GlobalId = globalId.ToString(),
                Subject = globalId.ToString()
            };
            var user =  _userRepository.Add(result);
            _userRepository.SaveChanges();
            return Task.FromResult(user.Id);
        }

        public Task<UserEditModel> SubmitEdit(UserEditModel model, int userId, string passwordDefault, CancellationToken cancellationToken = default)
        {
            var userEntity = _userRepository.Get(p => p.Id == model.Id).First();
            userEntity.FirstName = model.FirstName;
            userEntity.LastName = model.LastName;
            userEntity.IsActive = model.IsActive;
            if (model.Password != passwordDefault)
            {
                userEntity.Password = model.Password;
            }

            userEntity.RoleId = model.RoleId;
            userEntity.PhoneNumber = model.PhoneNumber;

            IsExistUserName(model.UserName);
            userEntity.UserName = model.UserName;
            _userRepository.Update(userEntity);
            _userRepository.SaveChanges();
            return Task.FromResult(Mapper.Map<UserEditModel>(userEntity));
        }

        public Task RemoveAsync(int id, int userId, CancellationToken cancellationToken = default)
        {
            var userEntity = _userRepository.Get(p => p.Id == id).First();
            _userRepository.Delete(userEntity);
            _userRepository.SaveChanges();
            return Task.CompletedTask;
        }

        #region Private

        private void IsExistUserName(string userName)
        {
            var isExitst = _userRepository.Get(p => p.UserName.ToUpper().Equals(userName.ToUpper()));
            if (isExitst.Count() > 1)
            {
                throw new BunnyException(ErrorCode.UserNameExist);
            }
        }

        #endregion Private
    }
}