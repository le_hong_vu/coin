﻿using System.Threading;
using System.Threading.Tasks;
using Bunny.DataTable.Models.Request;
using Bunny.DataTable.Models.Response;
using Bunny.Model.Coin;

namespace Bunny.Business.Interface.Coin
{
    public interface ICoinBusiness
    {
        Task<DataTableResponseDataModel<CoinDataTableModel>> GetDataTableAsync(DataTableParamModel model
            , CancellationToken cancellationToken = default);

        Task Active(int id, CancellationToken cancellationToken = default);
        Task SetDefaultDisableTimeCoin();
        Task ChangeMinDisable(int id, int numberChange);
    }
}