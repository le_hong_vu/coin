﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Bunny.DataTable.Models.Request;
using Bunny.DataTable.Models.Response;
using Bunny.Model.Binance;
using Bunny.Model.Coin;
using Bunny.Model.User;

namespace Bunny.Business.Interface.CoinManager
{
    public interface ICoinManagerBusiness
    {
        Task<DataTableResponseDataModel<NotificationCoinDataTableModel>> GetDataTableAsync(DataTableParamModel model, CancellationToken cancellationToken = default);
        Task<List<CoinViewModel>> GetListSymbolAsync();
        Task SendValueNotificationAsync();
        Task StartJob(int settingId);

        Task WriteLogTest();
    }
}