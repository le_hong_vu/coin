﻿using System.Threading;
using System.Threading.Tasks;
using Bunny.Middle.Model.User;

namespace Bunny.Business.Interface.MasterPage
{
    public interface IMasterPageBusiness
    {
        LoggedInUserModel SignIn(string userName, out string refreshToken, int? clientId);
        Task<bool> CheckValidSignIn(string userName, string password);
        void CheckValidRefreshToken(string refreshToken, int? clientId);

        Task<LoggedInUserModel> GetLoggedInUserByRefreshTokenAsync(string refreshToken,
            CancellationToken cancellationToken = default);
        Task<LoggedInUserModel> GetLoggedInUserBySubjectAsync(string subject, CancellationToken cancellationToken = default);
    }
}