﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bunny.Data.EF.Entities;
using Bunny.Model.SettingBinance;

namespace Bunny.Business.Interface.SettingBinance
{
    public interface ISettingBinanceBusiness
    {
        Task<List<SettingBinanceEditModel>> GetSettingBinance();
        Task<SettingBinanceEditModel> EditSettingBinance(SettingBinanceEditModel model, int userCurrentId);
        Task<List<SettingBinanceEntity>> GetAllSettingBinanceEntity();
    }
}