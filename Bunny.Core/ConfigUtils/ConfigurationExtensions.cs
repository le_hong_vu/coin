﻿using System;
using Bunny.Core.EnvironmentUtils;
using Microsoft.Extensions.Configuration;

namespace Bunny.Core.ConfigUtils
{
    public static class ConfigurationExtensions
    {
        /// <summary>
        ///     Gets a configuration sub-section with the specified key and binds it with the
        ///     specified type.
        /// </summary>
        /// <typeparam name="T"> The type of the configuration section to bind to. </typeparam>
        /// <param name="configuration"> The configuration. </param>
        /// <param name="key">          
        ///     The section key. If <c> null </c>, the name of the type <typeparamref name="T" /> is used.
        /// </param>
        /// <returns> The bound object. </returns>
        public static T GetSection<T>(this IConfiguration configuration, string key = null) where T : new()
        {
            if (string.IsNullOrWhiteSpace(key))
                key = typeof(T).Name;

            var value = new T();
            configuration.GetSection(key).Bind(value);
            return value;
        }

        /// <summary>
        ///     Production, Staging will read from section:"Environment Name", else by MachineName
        ///     (if not have data read from section:"Environment Name" key)
        /// </summary>
        /// <typeparam name="T"> The type to convert the value to. </typeparam>
        /// <param name="configuration"></param>
        /// <param name="section">       The configuration section for the value to convert. </param>
        /// <returns></returns>
        public static T GetValueByMachineAndEnv<T>(this IConfiguration configuration, string section)
        {
            if (string.IsNullOrWhiteSpace(section))
            {
                throw new ArgumentException($"{nameof(section)} cannot be null or empty", nameof(section));
            }

            T value;

            if (EnvironmentHelper.IsProduction() || EnvironmentHelper.IsStaging())
            {
                value = configuration.GetValue<T>($"{section}:{EnvironmentHelper.Name}");
            }
            else
            {
                var environmentName = !string.IsNullOrWhiteSpace(EnvironmentHelper.Name) ? EnvironmentHelper.Name : EnvironmentHelper.Development;

                var defaultValue = configuration.GetValue<T>($"{section}:{environmentName}");

                value = configuration.GetValue($"{section}:{EnvironmentHelper.MachineName}", defaultValue);
                //if (value == null)
                //{
                //    value = configuration.GetValue($"{section}:{"103.28.39.11"}", defaultValue);
                //}
            }

            return value;
        }
    }
}