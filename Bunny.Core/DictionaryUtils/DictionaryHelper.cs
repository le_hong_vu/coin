﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bunny.Core.ObjectUtils;
using Newtonsoft.Json;

namespace Bunny.Core.DictionaryUtils
{
    public static class DictionaryHelper
    {
        public static Dictionary<string, string> ToDictionary<T>(T data)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            List<string> properties = typeof(T).GetProperties().Select(x => x.Name).ToList();

            foreach (var propertyname in properties)
            {
                var propertyData = data.GetPropertyValue(propertyname);

                var dataStr = Convert.GetTypeCode(propertyData) != TypeCode.Object
                    ? propertyData as string
                    : JsonConvert.SerializeObject(propertyData, Constants.StandardFormat.JsonSerializerSettings).Trim('"');

                if (!string.IsNullOrEmpty(dataStr))
                {
                    dictionary.Add(propertyname, dataStr);
                }
            }

            return dictionary;
        }
    }
}