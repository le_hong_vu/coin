﻿using System.IO;
using Microsoft.EntityFrameworkCore.Internal;

namespace Bunny.Core.FileUtils
{
    public static class DirectoryHelper
    {
        public static void CreateIfNotExist(params string[] paths)
        {
            foreach (var path in paths)
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
        }

        public static void Empty(params string[] paths)
        {
            foreach (var path in paths)
            {
                // Remove all files
                var listFileInDirectory = Directory.GetFiles(path);
                foreach (var filePath in listFileInDirectory)
                {
                    File.Delete(filePath);
                }

                // Remove all directories
                var listDirectoryInDirectory = Directory.GetDirectories(path);
                foreach (var directoryPath in listDirectoryInDirectory)
                {
                    Directory.Delete(directoryPath);
                }
            }
        }

        public static bool IsEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }
    }
}