﻿namespace Bunny.Core.HttpUtils.HttpDetection.Device
{
    public enum DeviceType
    {
        Unknown,
        Desktop,
        Tablet,
        Mobile
    }
}