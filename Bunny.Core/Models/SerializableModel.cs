﻿
using Bunny.Core.Constants;
using Newtonsoft.Json;

namespace Bunny.Core.Models
{
    public abstract class SerializableModel
    {
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, StandardFormat.JsonSerializerSettings);
        }
    }
}