﻿using Bunny.Data.EF.Entities;
using Bunny.Data.EF.Factory;
using Bunny.Data.EF.Interface;
using Bunny.Data.EF.MapConfiguration.Coin;
using Bunny.Data.EF.MapConfiguration.NotificationCoin;
using Bunny.Data.EF.MapConfiguration.Role;
using Bunny.Data.EF.MapConfiguration.SettingBinance;
using Bunny.Data.EF.MapConfiguration.User;
using Bunny.DependencyInjection.Attributes;
using Bunny.EF;
using Bunny.EF.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations.Internal;

namespace Bunny.Data.EF
{
    [PerResolveDependency(ServiceType = typeof(IDbContext))]
    public sealed partial class DbContext : BaseDbContext, IDbContext
    {
        /// <summary>
        ///     Set CMD timeout is 20 minutes 
        /// </summary>
        public readonly int CmdTimeoutInSecond = 12000;

        public DbContext()
        {
            Database.SetCommandTimeout(CmdTimeoutInSecond);
        }
        public DbContext(string connectionString)
        {
            InitialDbContext(false);
        }
        private void InitialDbContext(bool enableConfig = true)
        {

        }
        //Start DB Set
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<RefreshTokenEntity> RefreshTokens { get; set; }
        public DbSet<RoleEntity> Roles { get; set; }
        public DbSet<NotificationCoinEntity> NotificationCoins { get; set; }
        public DbSet<SettingBinanceEntity> SettingBinances { get; set; }
        public DbSet<CoinEntity> Coins { get; set; }
        //End DB Set

        public DbContext(DbContextOptions<DbContext> options) : base(options)
        {
            Database.SetCommandTimeout(CmdTimeoutInSecond);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                DbContextFactory.GetDbContextBuilder(optionsBuilder);
            }


        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // [Important] Keep Under Base For Override And Make End Result

            // Scan and apply Config/Mapping for Tables/Entities (from folder "Map")
            builder.AddConfigFromAssembly<DbContext>(DbContextFactory.GetMigrationAssembly());

            // Set Delete Behavior as Restrict in Relationship
            builder.DisableCascadingDelete();

            // Convention for Table name
            builder.RemovePluralizingTableNameConvention();

            builder.ReplaceTableNameConvention("Entity", string.Empty);

            //reference entity
            builder.AddConfiguration(new UserConfiguration());
            builder.AddConfiguration(new RoleConfiguration());
            builder.AddConfiguration(new NotificationCoinConfiguration());
            builder.AddConfiguration(new SettingBinanceConfiguration());
            builder.AddConfiguration(new CoinConfiguration());
        }
        //public void InitializeDatabase()
        //{
        //    var connectionString = "BunnyEntity";
        //    using (var dbContext = new DbContext(connectionString))
        //    {
        //        dbContext.InitializeDatabase(dbContext);
        //    }
        //}
        //public void InitializeDatabase(IDbContext context)
        //{
        //    //var migrator = new Migrator();
        //    //migrator.Migrate();
        //}
    }
}