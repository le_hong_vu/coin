﻿using System;
using Bunny.Data.EF.Interface;
using Bunny.Middle;

namespace Bunny.Data.EF.Entities.Base
{
    public class Entity : Bunny.EF.Entities.Entity
    {
        public Entity()
        {
            CreatedTime = SystemUtils.SystemTimeNow;

            LastUpdatedTime = SystemUtils.SystemTimeNow;
        }
    }
}