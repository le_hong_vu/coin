﻿using Bunny.Data.EF.Entities.Base;
using Hangfire.Annotations;

namespace Bunny.Data.EF.Entities
{
    public class CoinEntity : Entity
    {
        public string Symbol { get; set; }
        public string BidPrice { get; set; }
        public string BidQty { get; set; }
        public string AskPrice { get; set; }
        public string AskQty { get; set; }
        public bool IsActive { get; set; }
        public int? SettingBinanceId { get; set; }
        public int MinDisableNotification { get; set; }

        #region virtual

        [CanBeNull] public virtual SettingBinanceEntity SettingBinance { get; set; }

        #endregion
    }
}