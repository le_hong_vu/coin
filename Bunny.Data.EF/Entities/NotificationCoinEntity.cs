﻿using System;
using Bunny.Data.EF.Entities.Base;

namespace Bunny.Data.EF.Entities
{
    public class NotificationCoinEntity : Entity
    {
        public string Name { get; set; }
        public string Symbol { get; set; }

        public decimal CurrentValuePrice { get; set; }
        public decimal AvgValuePrice { get; set; }
        public decimal IncreasePercentPrice { get; set; }

        public decimal CurrentValueVolume { get; set; }
        public decimal AvgValueVolume { get; set; }
        public decimal IncreasePercentVolume { get; set; }

        public decimal CurrentValueVolumeMax { get; set; }
        public decimal AvgValueVolumeMax { get; set; }
        public decimal IncreasePercentVolumeMax { get; set; }

        public decimal HighestPrice { get; set; }
        public decimal LowestPrice { get; set; }
        public decimal QuoteVolume { get; set; }

        public int UserId { get; set; }

        public DateTime TimeNotification { get; set; }
        public virtual UserEntity User { get; set; }
    }
}