﻿using System.Collections.Generic;
using Bunny.Data.EF.Entities.Base;
using Bunny.Middle;
using Bunny.Model;

namespace Bunny.Data.EF.Entities
{
    public class SettingBinanceEntity : Entity
    {
        public double LimitPrice { get; set; }
        public double DesirePricePercent { get; set; }
        public double PriceZ { get; set; }

        public double LimitVolume { get; set; }
        public double DesireVolumePercent { get; set; }
        public double VolumeZ { get; set; }

        public double LimitMaxVolume { get; set; }
        public double DesireMaxVolPercent { get; set; }
        public double MaxVolZ { get; set; }

        public EnumsApp.IntervalSetting Interval { get; set; }

        public bool IsCheckPrice { get; set; }
        public bool IsCheckVolume { get; set; }
        public bool IsCheckMaxVolume { get; set; }

        public int IndexMaxVolume { get; set; }

        public double SafePercentForBuy { get; set; }
        public int TotalColumnForBuy { get; set; }

        public double SafePercentForSell { get; set; }
        public int TotalColumnForSell { get; set; }

        public string TokenBotTelegram { get; set; }
        public string RoomIdBotTelegram { get; set; }


        public virtual ICollection<CoinEntity> Coin { get; set; }
    }
}