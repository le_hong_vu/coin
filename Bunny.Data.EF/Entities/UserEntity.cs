﻿using System.Collections.Generic;
using Bunny.Data.EF.Entities.Base;

namespace Bunny.Data.EF.Entities
{
    public class UserEntity : Entity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string PhoneNumber { get; set; }
        public string Subject { get; set; }

        public int RoleId { get; set; }

        #region virtual

        public virtual RoleEntity Role { get; set; }
        public virtual ICollection<NotificationCoinEntity> NotificationCoins { get; set; }

        #endregion
    }
}