﻿using System;
using Bunny.Data.EF.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Bunny.Data.EF.Extensions
{
    public static class MigrationExtensions
    {
        /// <summary>
        ///     <para>
        ///         Applies any pending migrations for the context to the database. Will create the
        ///         database if it does not already exist.
        ///     </para>
        /// </summary>
        public static IServiceProvider MigrateDatabase(this IServiceProvider service)
        {
            IDatabaseFactory databaseFactory = service.GetService<IDatabaseFactory>();
            return databaseFactory.MigrateDatabase(service);
        }

        /// <summary>
        ///     <para>
        ///         Applies any pending migrations for the context to the database. Will create the
        ///         database if it does not already exist.
        ///     </para>
        /// </summary>
        public static IApplicationBuilder MigrateDatabase(this IApplicationBuilder app)
        {
            IDatabaseFactory databaseFactory = app.ApplicationServices.GetService<IDatabaseFactory>();
            return databaseFactory.MigrateDatabase(app);
        }
    }
}