﻿using Bunny.Middle.Configs;
using Microsoft.Extensions.DependencyInjection;

namespace Bunny.Data.EF.Factory
{
    public static class DatabaseExtensions
    {
        /// <summary>
        ///     [Database] Use Entity Framework 
        /// </summary>
        /// <param name="services"></param>
        public static IServiceCollection AddDatabase(this IServiceCollection services)
        {
            services.AddDbContext<DbContext>(builder => DbContextFactory.GetDbContextBuilder(builder));

            //if (SystemConfig.IsUseLogDatabase)
            //{
            //    services.AddDbContext<LogDbContext>(builder => LogDbContextFactory.GetLogDbContextBuilder(builder));
            //}

            return services;
        }
    }
}