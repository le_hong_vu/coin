﻿using System.Reflection;
using Bunny.Core.ConfigUtils;
using Bunny.Data.EF.Interface;
using Bunny.Middle.Configs;
using Bunny.Middle.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;

namespace Bunny.Data.EF.Factory
{
    public class DbContextFactory : IDesignTimeDbContextFactory<DbContext>
    {
        public DbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DbContext>();

            builder.UseSqlServer(GetConnectionString(), ConfigOptionBuilder);

            return new DbContext(builder.Options);
        }

        public static DbContextOptionsBuilder GetDbContextBuilder(DbContextOptionsBuilder builder = null)
        {
            builder = builder ?? new DbContextOptionsBuilder();

            builder.UseSqlServer(GetConnectionString(), ConfigOptionBuilder);

            return builder;
        }

        public static void ConfigOptionBuilder(SqlServerDbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.MigrationsAssembly(GetMigrationAssemblyName());

            // Enable use Row No for Paging is needed unless you are on MSSQL 2012 or higher
            optionsBuilder.UseRowNumberForPaging(useRowNumberForPaging: false);
        }

        /// <summary>
        ///     Return connection string of current environment and machine
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString()
        {
            if (!string.IsNullOrWhiteSpace(SystemConfig.DatabaseConnectionString))
            {
                return SystemConfig.DatabaseConnectionString;
            }

            IConfigurationRoot config = new ConfigurationBuilder().AddJsonFile(Configuration.AppSettingsJsonFileName, false, true).Build();
            var connectionString = config.GetValueByMachineAndEnv<string>(Configuration.ConnectionStringsConfigSection);
            //if (connectionString == null)
            //{
            //    connectionString = "Data Source=103.28.39.11;Initial Catalog=nhvie2lz_Coin;User ID=nhvie2lz_Coin;Password=V4!Lh@#$;Trusted_Connection=False;MultipleActiveResultSets=True;";

            //}
            return connectionString;
        }

        public static Assembly GetMigrationAssembly()
        {
            return typeof(IDatabase).GetTypeInfo().Assembly;
        }

        public static string GetMigrationAssemblyName()
        {
            return typeof(IDatabase).GetTypeInfo().Assembly.GetName().Name;
        }
    }
}