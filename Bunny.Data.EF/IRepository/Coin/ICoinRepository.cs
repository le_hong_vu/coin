﻿using Bunny.Data.EF.Entities;

namespace Bunny.Data.EF.IRepository.Coin
{
    public interface ICoinRepository : IEntityRepository<CoinEntity>
    {
        
    }
}