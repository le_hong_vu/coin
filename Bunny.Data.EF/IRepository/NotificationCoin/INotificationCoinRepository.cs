﻿using Bunny.Data.EF.Entities;

namespace Bunny.Data.EF.IRepository.NotificationCoin
{
    public interface INotificationCoinRepository : IEntityRepository<NotificationCoinEntity>
    {
        
    }
}