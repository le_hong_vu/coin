﻿using Bunny.Data.EF.Entities;

namespace Bunny.Data.EF.IRepository.SettingBinance
{
    public interface ISettingBinanceRepository : IEntityRepository<SettingBinanceEntity>
    {
        
    }
}