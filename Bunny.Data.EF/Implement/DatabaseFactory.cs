﻿using System;
using Bunny.Data.EF.Interface;
using Bunny.DependencyInjection.Attributes;
using Bunny.EF;
using Bunny.Middle.Configs;
using Microsoft.AspNetCore.Builder;

namespace Bunny.Data.EF.Implement
{
    [PerResolveDependency(ServiceType = typeof(IDatabaseFactory))]
    public class DatabaseFactory : IDatabaseFactory
    {
        public IServiceProvider MigrateDatabase(IServiceProvider services)
        {
            services.MigrateDatabase<IDbContext>();

            if (SystemConfig.IsUseLogDatabase)
            {
                services.MigrateDatabase<DbContext>();
            }

            return services;
        }

        public IApplicationBuilder MigrateDatabase(IApplicationBuilder app)
        {
            app.MigrateDatabase<IDbContext>();

            if (SystemConfig.IsUseLogDatabase)
            {
                app.MigrateDatabase<DbContext>();
            }

            return app;
        }
    }
}