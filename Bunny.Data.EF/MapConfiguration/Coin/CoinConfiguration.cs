﻿using Bunny.Data.EF.Entities;
using Bunny.EF.Extensions;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bunny.Data.EF.MapConfiguration.Coin
{
    public class CoinConfiguration : DbEntityConfiguration<CoinEntity>
    {
        public override void Configure(EntityTypeBuilder<CoinEntity> entity)
        {
            entity.HasKey(c => c.Id);

            entity.HasOne(c => c.SettingBinance).WithMany(c => c.Coin).HasForeignKey(c => c.SettingBinanceId);
        }
    }
}