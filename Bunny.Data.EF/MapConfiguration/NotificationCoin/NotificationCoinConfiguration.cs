﻿using Bunny.Data.EF.Entities;
using Bunny.EF.Extensions;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bunny.Data.EF.MapConfiguration.NotificationCoin
{
    public class NotificationCoinConfiguration : DbEntityConfiguration<NotificationCoinEntity>
    {
        public override void Configure(EntityTypeBuilder<NotificationCoinEntity> entity)
        {
            entity.HasKey(c => c.Id);

            entity.HasOne(c => c.User).WithMany(c => c.NotificationCoins).HasForeignKey(c => c.UserId);
        }
    }
}