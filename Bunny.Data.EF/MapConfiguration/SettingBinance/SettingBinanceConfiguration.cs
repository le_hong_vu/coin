﻿using Bunny.Data.EF.Entities;
using Bunny.EF.Extensions;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bunny.Data.EF.MapConfiguration.SettingBinance
{
    public class SettingBinanceConfiguration : DbEntityConfiguration<SettingBinanceEntity>
    {
        public override void Configure(EntityTypeBuilder<SettingBinanceEntity> entity)
        {
            entity.HasKey(c => c.Id);

            entity.HasMany(c => c.Coin).WithOne(c => c.SettingBinance).HasForeignKey(c => c.SettingBinanceId);
        }
    }
}