﻿using Bunny.Data.EF.Entities;
using Bunny.EF.Extensions;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bunny.Data.EF.MapConfiguration.User
{
    public class UserConfiguration : DbEntityConfiguration<UserEntity>
    {
        public override void Configure(EntityTypeBuilder<UserEntity> entity)
        {
            entity.HasKey(c => c.Id);

            entity.HasOne(c => c.Role).WithMany(c => c.User).HasForeignKey(c => c.RoleId);

            entity.HasMany(c => c.NotificationCoins).WithOne(c => c.User).HasForeignKey(c => c.UserId);
        }
    }
}