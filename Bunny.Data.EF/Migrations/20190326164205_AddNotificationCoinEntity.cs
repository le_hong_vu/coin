﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bunny.Data.EF.Migrations
{
    public partial class AddNotificationCoinEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NotificationCoin",
                columns: table => new
                {
                    GlobalId = table.Column<string>(nullable: true),
                    CreatedTime = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedTime = table.Column<DateTimeOffset>(nullable: false),
                    DeletedTime = table.Column<DateTimeOffset>(nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(nullable: true),
                    LastUpdatedBy = table.Column<int>(nullable: true),
                    DeletedBy = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Symbol = table.Column<string>(nullable: true),
                    CurrentValuePrice = table.Column<double>(nullable: false),
                    AvgValuePrice = table.Column<double>(nullable: false),
                    IncreasePercentPrice = table.Column<double>(nullable: false),
                    CurrentValueVolume = table.Column<double>(nullable: false),
                    AvgValueVolume = table.Column<double>(nullable: false),
                    IncreasePercentVolume = table.Column<double>(nullable: false),
                    CurrentValueVolumeMax = table.Column<double>(nullable: false),
                    AvgValueVolumeMax = table.Column<double>(nullable: false),
                    IncreasePercentVolumeMax = table.Column<double>(nullable: false),
                    HighestPrice = table.Column<double>(nullable: false),
                    LowestPrice = table.Column<double>(nullable: false),
                    QuoteVolume = table.Column<double>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationCoin", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NotificationCoin_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NotificationCoin_UserId",
                table: "NotificationCoin",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NotificationCoin");
        }
    }
}
