﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bunny.Data.EF.Migrations
{
    public partial class AddSettingBinanceEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SettingBinance",
                columns: table => new
                {
                    GlobalId = table.Column<string>(nullable: true),
                    CreatedTime = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedTime = table.Column<DateTimeOffset>(nullable: false),
                    DeletedTime = table.Column<DateTimeOffset>(nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(nullable: true),
                    LastUpdatedBy = table.Column<int>(nullable: true),
                    DeletedBy = table.Column<int>(nullable: true),
                    CurrentValuePrice = table.Column<double>(nullable: false),
                    AvgValuePrice = table.Column<double>(nullable: false),
                    IncreasePercentPrice = table.Column<double>(nullable: false),
                    CurrentValueVolume = table.Column<double>(nullable: false),
                    AvgValueVolume = table.Column<double>(nullable: false),
                    IncreasePercentVolume = table.Column<double>(nullable: false),
                    CurrentValueVolumeMax = table.Column<double>(nullable: false),
                    AvgValueVolumeMax = table.Column<double>(nullable: false),
                    IncreasePercentVolumeMax = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SettingBinance", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SettingBinance");
        }
    }
}
