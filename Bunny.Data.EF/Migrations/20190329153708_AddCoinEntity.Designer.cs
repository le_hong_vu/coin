﻿// <auto-generated />
using System;
using Bunny.Data.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Bunny.Data.EF.Migrations
{
    [DbContext(typeof(DbContext))]
    [Migration("20190329153708_AddCoinEntity")]
    partial class AddCoinEntity
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Bunny.Data.EF.Entities.CoinEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("AskPrice");

                    b.Property<string>("AskQty");

                    b.Property<string>("BidPrice");

                    b.Property<string>("BidQty");

                    b.Property<int?>("CreatedBy");

                    b.Property<DateTimeOffset>("CreatedTime");

                    b.Property<int?>("DeletedBy");

                    b.Property<DateTimeOffset?>("DeletedTime");

                    b.Property<string>("GlobalId");

                    b.Property<int?>("LastUpdatedBy");

                    b.Property<DateTimeOffset>("LastUpdatedTime");

                    b.Property<string>("Symbol");

                    b.HasKey("Id");

                    b.ToTable("Coin");
                });

            modelBuilder.Entity("Bunny.Data.EF.Entities.NotificationCoinEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<double>("AvgValuePrice");

                    b.Property<double>("AvgValueVolume");

                    b.Property<double>("AvgValueVolumeMax");

                    b.Property<int?>("CreatedBy");

                    b.Property<DateTimeOffset>("CreatedTime");

                    b.Property<double>("CurrentValuePrice");

                    b.Property<double>("CurrentValueVolume");

                    b.Property<double>("CurrentValueVolumeMax");

                    b.Property<int?>("DeletedBy");

                    b.Property<DateTimeOffset?>("DeletedTime");

                    b.Property<string>("GlobalId");

                    b.Property<double>("HighestPrice");

                    b.Property<double>("IncreasePercentPrice");

                    b.Property<double>("IncreasePercentVolume");

                    b.Property<double>("IncreasePercentVolumeMax");

                    b.Property<int?>("LastUpdatedBy");

                    b.Property<DateTimeOffset>("LastUpdatedTime");

                    b.Property<double>("LowestPrice");

                    b.Property<string>("Name");

                    b.Property<double>("QuoteVolume");

                    b.Property<string>("Symbol");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("NotificationCoin");
                });

            modelBuilder.Entity("Bunny.Data.EF.Entities.RefreshTokenEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AccuracyRadius");

                    b.Property<string>("BrowserName");

                    b.Property<string>("BrowserVersion");

                    b.Property<int?>("CityGeoNameId");

                    b.Property<string>("CityName");

                    b.Property<string>("ContinentCode");

                    b.Property<int?>("ContinentGeoNameId");

                    b.Property<string>("ContinentName");

                    b.Property<int?>("CountryGeoNameId");

                    b.Property<string>("CountryIsoCode");

                    b.Property<string>("CountryName");

                    b.Property<int?>("CreatedBy");

                    b.Property<DateTimeOffset>("CreatedTime");

                    b.Property<int?>("DeletedBy");

                    b.Property<DateTimeOffset?>("DeletedTime");

                    b.Property<string>("DeviceHash");

                    b.Property<int>("DeviceType");

                    b.Property<string>("EngineName");

                    b.Property<string>("EngineVersion");

                    b.Property<DateTimeOffset?>("ExpireOn");

                    b.Property<string>("GlobalId");

                    b.Property<string>("IpAddress");

                    b.Property<int?>("LastUpdatedBy");

                    b.Property<DateTimeOffset>("LastUpdatedTime");

                    b.Property<double?>("Latitude");

                    b.Property<double?>("Longitude");

                    b.Property<string>("MarkerName");

                    b.Property<string>("MarkerVersion");

                    b.Property<string>("OsName");

                    b.Property<string>("OsVersion");

                    b.Property<string>("PostalCode");

                    b.Property<string>("RefreshToken");

                    b.Property<string>("TimeZone");

                    b.Property<int>("TotalUsage");

                    b.Property<string>("UserAgent");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("RefreshToken");
                });

            modelBuilder.Entity("Bunny.Data.EF.Entities.RoleEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("CreatedBy");

                    b.Property<DateTimeOffset>("CreatedTime");

                    b.Property<int?>("DeletedBy");

                    b.Property<DateTimeOffset?>("DeletedTime");

                    b.Property<string>("Description");

                    b.Property<string>("GlobalId");

                    b.Property<int?>("LastUpdatedBy");

                    b.Property<DateTimeOffset>("LastUpdatedTime");

                    b.Property<string>("Name");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("Role");
                });

            modelBuilder.Entity("Bunny.Data.EF.Entities.SettingBinanceEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<double>("AvgValuePrice");

                    b.Property<double>("AvgValueVolume");

                    b.Property<double>("AvgValueVolumeMax");

                    b.Property<int?>("CreatedBy");

                    b.Property<DateTimeOffset>("CreatedTime");

                    b.Property<double>("CurrentValuePrice");

                    b.Property<double>("CurrentValueVolume");

                    b.Property<double>("CurrentValueVolumeMax");

                    b.Property<int?>("DeletedBy");

                    b.Property<DateTimeOffset?>("DeletedTime");

                    b.Property<string>("GlobalId");

                    b.Property<double>("IncreasePercentPrice");

                    b.Property<double>("IncreasePercentVolume");

                    b.Property<double>("IncreasePercentVolumeMax");

                    b.Property<int?>("LastUpdatedBy");

                    b.Property<DateTimeOffset>("LastUpdatedTime");

                    b.HasKey("Id");

                    b.ToTable("SettingBinance");
                });

            modelBuilder.Entity("Bunny.Data.EF.Entities.UserEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("CreatedBy");

                    b.Property<DateTimeOffset>("CreatedTime");

                    b.Property<int?>("DeletedBy");

                    b.Property<DateTimeOffset?>("DeletedTime");

                    b.Property<string>("FirstName");

                    b.Property<string>("GlobalId");

                    b.Property<bool>("IsActive");

                    b.Property<string>("LastName");

                    b.Property<int?>("LastUpdatedBy");

                    b.Property<DateTimeOffset>("LastUpdatedTime");

                    b.Property<string>("Password");

                    b.Property<string>("PhoneNumber");

                    b.Property<int>("RoleId");

                    b.Property<string>("Subject");

                    b.Property<string>("UserName");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("User");
                });

            modelBuilder.Entity("Bunny.Data.EF.Entities.NotificationCoinEntity", b =>
                {
                    b.HasOne("Bunny.Data.EF.Entities.UserEntity", "User")
                        .WithMany("NotificationCoins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Bunny.Data.EF.Entities.RefreshTokenEntity", b =>
                {
                    b.HasOne("Bunny.Data.EF.Entities.UserEntity", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Bunny.Data.EF.Entities.UserEntity", b =>
                {
                    b.HasOne("Bunny.Data.EF.Entities.RoleEntity", "Role")
                        .WithMany("User")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Restrict);
                });
#pragma warning restore 612, 618
        }
    }
}
