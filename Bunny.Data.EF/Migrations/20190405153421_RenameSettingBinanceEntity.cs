﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bunny.Data.EF.Migrations
{
    public partial class RenameSettingBinanceEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IncreasePercentVolumeMax",
                table: "SettingBinance",
                newName: "VolumeZ");

            migrationBuilder.RenameColumn(
                name: "IncreasePercentVolume",
                table: "SettingBinance",
                newName: "PriceZ");

            migrationBuilder.RenameColumn(
                name: "IncreasePercentPrice",
                table: "SettingBinance",
                newName: "MaxVolZ");

            migrationBuilder.RenameColumn(
                name: "CurrentValueVolumeMax",
                table: "SettingBinance",
                newName: "LimitVolume");

            migrationBuilder.RenameColumn(
                name: "CurrentValueVolume",
                table: "SettingBinance",
                newName: "LimitPrice");

            migrationBuilder.RenameColumn(
                name: "CurrentValuePrice",
                table: "SettingBinance",
                newName: "LimitMaxVolume");

            migrationBuilder.RenameColumn(
                name: "AvgValueVolumeMax",
                table: "SettingBinance",
                newName: "DesireVolumePercent");

            migrationBuilder.RenameColumn(
                name: "AvgValueVolume",
                table: "SettingBinance",
                newName: "DesirePricePercent");

            migrationBuilder.RenameColumn(
                name: "AvgValuePrice",
                table: "SettingBinance",
                newName: "DesireMaxVolPercent");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "VolumeZ",
                table: "SettingBinance",
                newName: "IncreasePercentVolumeMax");

            migrationBuilder.RenameColumn(
                name: "PriceZ",
                table: "SettingBinance",
                newName: "IncreasePercentVolume");

            migrationBuilder.RenameColumn(
                name: "MaxVolZ",
                table: "SettingBinance",
                newName: "IncreasePercentPrice");

            migrationBuilder.RenameColumn(
                name: "LimitVolume",
                table: "SettingBinance",
                newName: "CurrentValueVolumeMax");

            migrationBuilder.RenameColumn(
                name: "LimitPrice",
                table: "SettingBinance",
                newName: "CurrentValueVolume");

            migrationBuilder.RenameColumn(
                name: "LimitMaxVolume",
                table: "SettingBinance",
                newName: "CurrentValuePrice");

            migrationBuilder.RenameColumn(
                name: "DesireVolumePercent",
                table: "SettingBinance",
                newName: "AvgValueVolumeMax");

            migrationBuilder.RenameColumn(
                name: "DesirePricePercent",
                table: "SettingBinance",
                newName: "AvgValueVolume");

            migrationBuilder.RenameColumn(
                name: "DesireMaxVolPercent",
                table: "SettingBinance",
                newName: "AvgValuePrice");
        }
    }
}
