﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bunny.Data.EF.Migrations
{
    public partial class UpdateSettingBinanceEntity1542019 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IndexMaxVolume",
                table: "SettingBinance",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsCheckMaxVolume",
                table: "SettingBinance",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsCheckPrice",
                table: "SettingBinance",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsCheckVolume",
                table: "SettingBinance",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "RoomIdBotTelegram",
                table: "SettingBinance",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "SafePercentForBuy",
                table: "SettingBinance",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "SafePercentForSell",
                table: "SettingBinance",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "TokenBotTelegram",
                table: "SettingBinance",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TotalColumnForBuy",
                table: "SettingBinance",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalColumnForSell",
                table: "SettingBinance",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IndexMaxVolume",
                table: "SettingBinance");

            migrationBuilder.DropColumn(
                name: "IsCheckMaxVolume",
                table: "SettingBinance");

            migrationBuilder.DropColumn(
                name: "IsCheckPrice",
                table: "SettingBinance");

            migrationBuilder.DropColumn(
                name: "IsCheckVolume",
                table: "SettingBinance");

            migrationBuilder.DropColumn(
                name: "RoomIdBotTelegram",
                table: "SettingBinance");

            migrationBuilder.DropColumn(
                name: "SafePercentForBuy",
                table: "SettingBinance");

            migrationBuilder.DropColumn(
                name: "SafePercentForSell",
                table: "SettingBinance");

            migrationBuilder.DropColumn(
                name: "TokenBotTelegram",
                table: "SettingBinance");

            migrationBuilder.DropColumn(
                name: "TotalColumnForBuy",
                table: "SettingBinance");

            migrationBuilder.DropColumn(
                name: "TotalColumnForSell",
                table: "SettingBinance");
        }
    }
}
