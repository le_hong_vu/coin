﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bunny.Data.EF.Migrations
{
    public partial class AddIsActiveFieldForCoinEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Coin",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Coin");
        }
    }
}
