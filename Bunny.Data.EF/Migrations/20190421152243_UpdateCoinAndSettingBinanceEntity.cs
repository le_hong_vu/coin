﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bunny.Data.EF.Migrations
{
    public partial class UpdateCoinAndSettingBinanceEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SettingBinanceId",
                table: "Coin",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Coin_SettingBinanceId",
                table: "Coin",
                column: "SettingBinanceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Coin_SettingBinance_SettingBinanceId",
                table: "Coin",
                column: "SettingBinanceId",
                principalTable: "SettingBinance",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Coin_SettingBinance_SettingBinanceId",
                table: "Coin");

            migrationBuilder.DropIndex(
                name: "IX_Coin_SettingBinanceId",
                table: "Coin");

            migrationBuilder.DropColumn(
                name: "SettingBinanceId",
                table: "Coin");
        }
    }
}
