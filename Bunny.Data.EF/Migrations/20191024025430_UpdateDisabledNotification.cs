﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bunny.Data.EF.Migrations
{
    public partial class UpdateDisabledNotification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MinDisableNotification",
                table: "SettingBinance",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "TimeNotification",
                table: "NotificationCoin",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MinDisableNotification",
                table: "SettingBinance");

            migrationBuilder.DropColumn(
                name: "TimeNotification",
                table: "NotificationCoin");
        }
    }
}
