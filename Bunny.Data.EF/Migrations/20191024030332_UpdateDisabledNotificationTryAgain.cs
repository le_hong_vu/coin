﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bunny.Data.EF.Migrations
{
    public partial class UpdateDisabledNotificationTryAgain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MinDisableNotification",
                table: "SettingBinance");

            migrationBuilder.AddColumn<int>(
                name: "MinDisableNotification",
                table: "Coin",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MinDisableNotification",
                table: "Coin");

            migrationBuilder.AddColumn<int>(
                name: "MinDisableNotification",
                table: "SettingBinance",
                nullable: false,
                defaultValue: 0);
        }
    }
}
