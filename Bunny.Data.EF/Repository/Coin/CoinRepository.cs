﻿using Bunny.Data.EF.Entities;
using Bunny.Data.EF.Implement;
using Bunny.Data.EF.Interface;
using Bunny.Data.EF.IRepository.Coin;
using Bunny.DependencyInjection.Attributes;

namespace Bunny.Data.EF.Repository.Coin
{
    [PerRequestDependency(ServiceType = typeof(ICoinRepository))]
    public class CoinRepository : EntityRepository<CoinEntity>, ICoinRepository
    {
        public CoinRepository(IDbContext dbContext) : base(dbContext)
        {
        }
    }
}