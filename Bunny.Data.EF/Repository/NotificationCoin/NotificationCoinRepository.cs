﻿using Bunny.Data.EF.Entities;
using Bunny.Data.EF.Implement;
using Bunny.Data.EF.Interface;
using Bunny.Data.EF.IRepository.NotificationCoin;
using Bunny.DependencyInjection.Attributes;

namespace Bunny.Data.EF.Repository.NotificationCoin
{
    [PerRequestDependency(ServiceType = typeof(INotificationCoinRepository))]
    public class NotificationCoinRepository : EntityRepository<NotificationCoinEntity>, INotificationCoinRepository
    {
        public NotificationCoinRepository(IDbContext dbContext) : base(dbContext)
        {
        }
    }
}