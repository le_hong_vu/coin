﻿using Bunny.Data.EF.Entities;
using Bunny.Data.EF.Implement;
using Bunny.Data.EF.Interface;
using Bunny.Data.EF.IRepository.RefreshToken;
using Bunny.DependencyInjection.Attributes;

namespace Bunny.Data.EF.Repository.RefreshToken
{
    [PerRequestDependency(ServiceType = typeof(IRefreshTokenRepository))]
    public class RefreshTokenRepository : EntityRepository<RefreshTokenEntity>, IRefreshTokenRepository
    {
        public RefreshTokenRepository(IDbContext dbContext) : base(dbContext)
        {
        }
    }
}