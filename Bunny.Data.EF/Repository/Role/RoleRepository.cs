﻿using Bunny.Data.EF.Entities;
using Bunny.Data.EF.Implement;
using Bunny.Data.EF.Interface;
using Bunny.Data.EF.IRepository.Role;
using Bunny.DependencyInjection.Attributes;

namespace Bunny.Data.EF.Repository.Role
{
    [PerRequestDependency(ServiceType = typeof(IRoleRepository))]
    public class RoleRepository : EntityRepository<RoleEntity>, IRoleRepository
    {
        public RoleRepository(IDbContext dbContext) : base(dbContext)
        {
        }
    }
}