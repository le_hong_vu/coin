﻿using Bunny.Data.EF.Entities;
using Bunny.Data.EF.Implement;
using Bunny.Data.EF.Interface;
using Bunny.Data.EF.IRepository.SettingBinance;
using Bunny.DependencyInjection.Attributes;

namespace Bunny.Data.EF.Repository.SettingBinance
{
    [PerRequestDependency(ServiceType = typeof(ISettingBinanceRepository))]
    public class SettingBinanceRepository : EntityRepository<SettingBinanceEntity>, ISettingBinanceRepository
    {
        public SettingBinanceRepository(IDbContext dbContext) : base(dbContext)
        {
        }
    }
}