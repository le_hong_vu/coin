﻿namespace Bunny.DataTable.Constants
{
    public enum DateTimeFormatMode
    {
        /// <summary>
        ///     Try parse DateTime from any string format 
        /// </summary>
        Auto,

        /// <summary>
        ///     Parse DateTime by specific/exactly format. 
        /// </summary>
        Specific
    }
}