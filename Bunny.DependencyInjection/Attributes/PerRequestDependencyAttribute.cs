﻿using Microsoft.Extensions.DependencyInjection;

namespace Bunny.DependencyInjection.Attributes
{
    public class PerRequestDependencyAttribute : DependencyAttribute
    {
        public PerRequestDependencyAttribute() : base(ServiceLifetime.Scoped)
        {
        }
    }
}