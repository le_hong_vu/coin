﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
namespace Bunny.DependencyInjection
{
    public static class Resolver
    {
        private static IServiceCollection _services;

        private static IServiceProvider _serviceProvider;

        /// <summary>
        ///     Static Service Collection of System 
        /// </summary>
        public static IServiceCollection Services
        {
            get => _services;
            set
            {
                _services = value;
                _serviceProvider = _services.BuildServiceProvider();
            }
        }

        /// <summary>
        ///     Static Service Provider of System 
        /// </summary>
        /// <remarks>
        ///     Priority to use <see cref="HttpContext.Current" /> to get RequestServices
        /// </remarks>
        public static IServiceProvider ServiceProvider => System.Web.HttpContext.Current?.RequestServices ?? _serviceProvider;

        public static T Resolve<T>() where T : class
        {
            return ServiceProvider.Resolve<T>();
        }
    }
}