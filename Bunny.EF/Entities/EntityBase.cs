﻿using System;
using Bunny.EF.Interfaces.Entities;

namespace Bunny.EF.Entities
{
    public abstract class EntityBase : IGlobalIdentityEntity, ISoftDeletableEntity, IAuditableEntity
    {
        public string GlobalId { get; set; } = Guid.NewGuid().ToString("N");

        public DateTimeOffset CreatedTime { get; set; } = DateTimeOffset.UtcNow;

        public DateTimeOffset LastUpdatedTime { get; set; } = DateTimeOffset.UtcNow;

        public DateTimeOffset? DeletedTime { get; set; }
    }
}