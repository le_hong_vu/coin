﻿using System;

namespace Bunny.EF.Interfaces.Entities
{
    public interface IAuditableEntity
    {
        DateTimeOffset CreatedTime { get; set; }

        DateTimeOffset LastUpdatedTime { get; set; }
    }

    /// <summary>
    ///     Audiable entity by CreatedTime and Nullable LastUpdatedTime 
    /// </summary>
    /// <typeparam name="TKey"> User Id </typeparam>
    public interface IAuditableEntity<TKey> : IAuditableEntity where TKey : struct
    {
        TKey? CreatedBy { get; set; }

        TKey? LastUpdatedBy { get; set; }
    }

    /// <summary>
    ///     Audiable entity by CreatedTime and Nullable LastUpdatedTime 
    /// </summary>
    public interface IAuditableEntityString : IAuditableEntity
    {
        string CreatedBy { get; set; }

        string LastUpdatedBy { get; set; }
    }
}