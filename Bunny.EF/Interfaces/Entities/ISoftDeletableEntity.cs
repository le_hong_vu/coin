﻿using System;

namespace Bunny.EF.Interfaces.Entities
{
    public interface ISoftDeletableEntity
    {
        DateTimeOffset? DeletedTime { get; set; }
    }

    /// <summary>
    ///     Soft Deletable Entity by IsDeleted as marker and Nullable DateTimeOffset as audit log 
    /// </summary>
    /// <typeparam name="TKey"> User Id </typeparam>
    public interface ISoftDeletableEntity<TKey> : ISoftDeletableEntity where TKey : struct
    {
        TKey? DeletedBy { get; set; }
    }

    /// <summary>
    ///     Soft Deletable Entity by IsDeleted as marker and Nullable DateTimeOffset as audit log 
    /// </summary>
    public interface ISoftDeletableEntityString : ISoftDeletableEntity
    {
        string DeletedBy { get; set; }
    }
}