﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bunny.EF.Maps
{
    public interface ITypeConfiguration<T> where T : class
    {
        void Map(EntityTypeBuilder<T> builder);
    }
}