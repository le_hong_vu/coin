﻿using Microsoft.AspNetCore.Http;

namespace Bunny.Hangfire
{
    public static class Helper
    {
        internal const string CookieAccessKeyName = "Hangfire_AccessKey";

        public static bool IsCanAccessHangfireDashboard(HttpContext httpContext)
        {
            if (string.IsNullOrWhiteSpace(HangfireConfig.AccessKey))
            {
                return true;
            }

            string requestKey = httpContext.Request.Query[HangfireConfig.AccessKeyQueryParam];

            requestKey = string.IsNullOrWhiteSpace(requestKey) ? httpContext.Request.Cookies[CookieAccessKeyName] : requestKey;

            var isCanAccess = HangfireConfig.AccessKey == requestKey;

            return isCanAccess;
        }
    }
}