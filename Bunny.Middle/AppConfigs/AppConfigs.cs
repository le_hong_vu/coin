﻿namespace Bunny.Middle.AppConfigs
{
    public static class AppConfigs
    {
        public static DataHashModel DataHashConfig { get; set; }
        public static BinanceModel BinanceConfig { get; set; }
        public static HangFireModel HangFireConfig { get; set; }
    }
}