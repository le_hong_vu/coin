﻿namespace Bunny.Middle.AppConfigs
{
    public class BinanceModel
    {
        public string klinesApiUrl { get; set; }
        public string price24hrUrl { get; set; }
        public string getSymbolsUrl { get; set; }
    }
}