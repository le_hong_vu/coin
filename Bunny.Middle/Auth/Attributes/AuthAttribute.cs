﻿using System;

namespace Bunny.Middle.Auth.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class AuthAttribute : Attribute
    {
        public Enums.Role[] Roles { get; }

        public AuthAttribute(params Enums.Role[] roles)
        {
            Roles = roles;
        }
    }
}