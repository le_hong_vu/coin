﻿using System;
using System.Text;
using Bunny.Core.StringUtils;
using Microsoft.IdentityModel.Tokens;

namespace Bunny.Middle.Auth
{
    public static class AuthConfig
    {
        private static string _secretKey;

        public static string SecretKey
        {
            get => _secretKey;
            set
            {
                StringHelper.CheckNullOrWhiteSpace(value);

                _secretKey = value;

                SecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
                SigningCredentials = new SigningCredentials(SecurityKey, SecurityAlgorithms.HmacSha256Signature);
            }
        }

        public static TimeSpan AccessTokenExpireIn { get; set; }

        internal static SymmetricSecurityKey SecurityKey { get; private set; }

        internal static SigningCredentials SigningCredentials { get; private set; }

        internal static TokenValidationParameters TokenValidationParameters => new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = SecurityKey,
            ValidateLifetime = true,
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidateActor = false,
            ClockSkew = SystemUtils.SystemTimeZoneInfo.BaseUtcOffset
        };
    }
}