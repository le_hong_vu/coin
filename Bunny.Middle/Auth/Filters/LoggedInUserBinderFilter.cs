﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace Bunny.Middle.Auth.Filters
{
    public class LoggedInUserBinderFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            LoggedInUserBinder.BindLoggedInUser(context.HttpContext);
        }
    }
}