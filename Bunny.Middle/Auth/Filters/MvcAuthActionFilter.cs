﻿using System.Linq;
using Bunny.Core.HttpUtils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Bunny.Middle.Auth.Filters
{
    public class MvcAuthActionFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (context.HttpContext.Request.IsAjaxRequest())
            {
                if (!context.IsAuthenticated())
                {
                    context.Result = new JsonResult(new { });
                    context.HttpContext.Response.StatusCode = StatusCodes.Status410Gone;
                    return;
                }

                if (!context.IsAuthorized())
                {
                    context.Result = new JsonResult(new { });
                    context.HttpContext.Response.StatusCode = StatusCodes.Status403Forbidden;
                    return;
                }

                return;
            }

            //Get url login page
            var urlLoginPage = context.HttpContext.Request.GetDomain() + new UrlHelper(context).Action("Index", "MasterPage");
            var isAllowAnonymous = context.Filters.Any(item => item is IAllowAnonymousFilter);
            if (!context.HttpContext.User.Identity.IsAuthenticated && context.HttpContext.Request.GetDisplayUrl() != urlLoginPage
                && !isAllowAnonymous)
            {
                //var redirectUrl = context.HttpContext.Request.GetDisplayUrl();
                context.Result = new RedirectToActionResult("Index", "MasterPage", new { area = ""}, false);
                return;
            }

            if (!context.IsAuthorized())
            {
                context.Result = new RedirectToActionResult("Index", "DashBoard", new { area = "web" });
            }
        }
    }
}