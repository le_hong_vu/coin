﻿using System;

namespace Bunny.Middle.Configs.Models.MvcPath
{
    public class StaticsContentConfigModel
    {
        /// <summary>
        ///     Area 
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        ///     Use exactly folder name case in explorer 
        /// </summary>
        /// <remarks> Relative path from <see cref="Area" /> </remarks>
        public string FolderRelativePath { get; set; }

        /// <summary>
        ///     Use lower case for http request 
        /// </summary>
        public string HttpRequestPath { get; set; }

        /// <summary>
        ///     Max Age in Cache Control Header 
        /// </summary>
        /// <remarks> Use the . separator between days and hours, see more: https://msdn.microsoft.com/en-us/library/system.timespan.aspx </remarks>
        public TimeSpan? MaxAgeResponseHeader { get; set; } = new TimeSpan(365, 0, 0, 0);
    }
}