﻿namespace Bunny.Middle.Constants
{
    public static class Constant
    {
        public const string DefaultConfigSection = "Authentication";

        public const string AuthenticationTokenType = "Bearer";

        public const string ClientIdKey = "client_id";


        public static class Coin
        {
            public const int DefaultTimeDisableCoin = 3;
        }
    }
}