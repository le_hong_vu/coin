﻿using Bunny.Core.StringUtils;

namespace Bunny.Middle.Constants
{
    public static class Culture
    {
        public static readonly string CookieName = $"{nameof(Bunny)}Culture".GetSha256();

        public const string English = "en-US";

        public const string Chinese = "cn-CN";
    }
}