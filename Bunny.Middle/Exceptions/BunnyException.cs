﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Bunny.Middle.Exceptions
{
    public class BunnyException : Exception
    {
        public BunnyException(ErrorCode code, string message = "") : base(message)
        {
            Code = code;
        }

        public BunnyException(ErrorCode code, Dictionary<string, object> additionalData) : this(code)
        {
            AdditionalData = additionalData;
        }

        public BunnyException(ErrorCode code, string message, Dictionary<string, object> additionalData) : this(code, message)
        {
            AdditionalData = additionalData;
        }

        public ErrorCode Code { get; }

        [JsonExtensionData]
        public Dictionary<string, object> AdditionalData { get; set; } = new Dictionary<string, object>();
    }
}