﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Bunny.Middle.Exceptions
{
    public enum ErrorCode
    {
        [Description("Bad Request")]
        [Display(GroupName = "Global")]
        BadRequest = 400,

        [Display(GroupName = "Global")]
        [Description("Un-Authenticate")]
        UnAuthenticated = 401,

        [Display(GroupName = "Global")]
        [Description("Forbidden")]
        UnAuthorized = 403,

        [Display(GroupName = "Global")]
        [Description("Not Found, the resource does not exist")]
        NotFound = 404,

        [Description("Oops! Something went wrong, please try again later")]
        [Display(GroupName = "Global")]
        Unknown = 500,

        [Description("Missing Configuration")]
        [Display(GroupName = "Global")]
        MissingConfiguration = 700,

        // Access Token

        [Display(GroupName = "Token")]
        [Description("Invalid refresh token")]
        InvalidRefreshToken = 600,

        [Display(GroupName = "Token")]
        [Description("Refresh token is expired")]
        RefreshTokenExpired = 601,

        [Display(GroupName = "Token")]
        [Description("Invalid access token")]
        InvalidAccessToken = 602,

        [Display(GroupName = "Token")]
        [Description("Access token is expired")]
        AccessTokenExpired = 603,

        // User Login
        [Display(GroupName = "User")]
        [Description("User is in-active")]
        UserInActive = 1000,

        [Display(GroupName = "User")]
        [Description("User Name is already exist")]
        UserNameNotUnique = 1001,

        [Display(GroupName = "User")]
        [Description("User password or user name is wrong")]
        UserPasswordOrUserNameWrong = 1002,

        // User manager
        [Display(GroupName = "Add User")]
        [Description("Role is empty")]
        RoleRequired = 1100,

        [Display(GroupName = "Edit User")]
        [Description("UserName is exist")]
        UserNameExist = 1101,
    }
}