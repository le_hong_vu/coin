﻿using System.Collections.Generic;
using Bunny.Model.User;

namespace Bunny.Middle.Model.User
{
    public class LoggedInUserModel : UserViewModel
    {
        public int Id { get; set; }
        public int? EmployeeId { get; set; }

        public int? ManageCompanyId { get; set; }

        public List<Enums.Role> ListRole { get; set; }

        public string AccessToken { get; set; }
        
        public string AccessTokenType { get; set; }

        public int RoleId { get; set; }

        /// <summary>
        ///     Check current logged in user have any permission in <see cref="permissions" /> 
        /// </summary>
        /// <param name="permissions"></param>
        /// <returns></returns>
        //public bool IsHaveAnyPermissions(params Enums.Permission[] permissions)
        //{
        //    if (permissions?.Any() != true)
        //    {
        //        return true;
        //    }

        //    return ListPermission?.Any() == true && permissions.Any(ListPermission.Contains);
        //}

        /// <summary>
        ///     Check current logged in user have all permission in <see cref="permissions" /> 
        /// </summary>
        /// <param name="permissions"></param>
        /// <returns></returns>
        //public bool IsHaveAllPermissions(params Enums.Permission[] permissions)
        //{
        //    if (permissions?.Any() != true)
        //    {
        //        return true;
        //    }

        //    return ListPermission?.Any() == true && permissions.All(ListPermission.Contains);
        //}

        /// <summary>
        ///     Check current logged in user have any permission in <see cref="permissions" /> 
        /// </summary>
        /// <param name="permissions"></param>
        /// <returns></returns>
        //public bool IsHaveAnyPermissions(IEnumerable<Enums.Permission> permissions)
        //{
        //    var listPermission = permissions?.ToList();

        //    if (listPermission?.Any() != true)
        //    {
        //        return true;
        //    }

        //    return ListPermission?.Any() == true && listPermission.Any(ListPermission.Contains);
        //}

        /// <summary>
        ///     Check current logged in user have all permission in <see cref="permissions" /> 
        /// </summary>
        /// <param name="permissions"></param>
        /// <returns></returns>
        //public bool IsHaveAllPermissions(IEnumerable<Enums.Permission> permissions)
        //{
        //    var listPermission = permissions?.ToList();

        //    if (listPermission?.Any() != true)
        //    {
        //        return true;
        //    }

        //    return ListPermission?.Any() == true && listPermission.All(ListPermission.Contains);
        //}
    }
}