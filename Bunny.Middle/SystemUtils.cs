﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using Bunny.Core.DateTimeUtils;
using Bunny.Core.FileUtils;
using Bunny.Middle.Configs;
using Enums = Bunny.Middle.Enums;
namespace Bunny.Middle
{
    public static class SystemUtils
    {
        public static TimeZoneInfo SystemTimeZoneInfo;

        public static DateTimeOffset SystemTimeNow => DateTimeOffset.UtcNow.UtcToSystemTime();

        #region Date Time

        /// <summary>
        ///     Null or less than system now will use system now value 
        /// </summary>
        /// <param name="dateTimeOffset"></param>
        /// <returns></returns>
        public static DateTimeOffset GetAtLeastSystemTimeNow(DateTimeOffset? dateTimeOffset)
        {
            var systemNow = SystemTimeNow;

            dateTimeOffset = dateTimeOffset ?? SystemTimeNow;

            var dateTimeAtLeastSystemNow = dateTimeOffset < systemNow ? systemNow : dateTimeOffset.Value;

            return dateTimeAtLeastSystemNow;
        }

        public static DateTime UtcToSystemTime(this DateTimeOffset dateTimeOffsetUtc)
        {
            return dateTimeOffsetUtc.UtcDateTime.GetDateTimeFromUtc(SystemTimeZoneInfo);
        }

        public static DateTime UtcToSystemTime(this DateTime dateTimeUtc)
        {
            return dateTimeUtc.GetDateTimeFromUtc(SystemTimeZoneInfo);
        }

        public static DateTimeOffset? ToSystemDateTime(this string dateTimeString)
        {
            DateTimeOffset result;

            if (DateTime.TryParseExact(dateTimeString, SystemConfig.SystemDateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out var dateTime))
            {
                result = dateTime;
            }
            else if (DateTime.TryParseExact(dateTimeString, SystemConfig.SystemDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out var date))
            {
                result = date;
            }
            else
            {
                return null;
            }

            result = result.WithTimeZone(SystemConfig.SystemTimeZone);

            return result;
        }

        #endregion

        public static Enums.BooleanNA ToBooleanNa(this bool? status)
        {
            switch (status)
            {
                case true:
                    return Enums.BooleanNA.Yes;
                case null:
                    return Enums.BooleanNA.NA;
                default:
                    return Enums.BooleanNA.No;
            }
        }

        public static string GetWebPhysicalPath(string path)
        {
            if (!Uri.TryCreate(path, UriKind.RelativeOrAbsolute, out var pathUri))
            {
                throw new ArgumentException($"Invalid path {path}");
            }

            if (pathUri.IsAbsoluteUri) return path;

            path = Path.Combine(SystemConfig.MvcPath.WebRootFolderName, path);

            return path;
        }

        public static string GetTaxFilePhysicalPath(string path)
        {
            if (!Uri.TryCreate(path, UriKind.RelativeOrAbsolute, out var pathUri))
            {
                throw new ArgumentException($"Invalid path {path}");
            }

            if (pathUri.IsAbsoluteUri) return path;
            return path;
        }

        public static void CopyFolder(string sourcePath, string destinationPath)
        {
            DirectoryHelper.CreateIfNotExist(destinationPath);
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(sourcePath, "*",
                SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(sourcePath, destinationPath));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(sourcePath, "*.*",
                SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(sourcePath, destinationPath), true);
        }


        public static void DeleteDirectory(string target_dir)
        {
            string[] files = Directory.GetFiles(target_dir);
            string[] dirs = Directory.GetDirectories(target_dir);

            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }

            Directory.Delete(target_dir, false);
        }

        public static string GetWebUrl(string path)
        {
            if (!Uri.TryCreate(path, UriKind.RelativeOrAbsolute, out var pathUri))
            {
                throw new ArgumentException($"Invalid path {path}");
            }

            if (pathUri.IsAbsoluteUri) return path;

            path = path.Replace(SystemConfig.MvcPath.WebRootFolderName, string.Empty).TrimStart('/').TrimStart('/').TrimStart('\\').TrimStart('\\');

            return path;
        }

        private static readonly int[] Multiples = { 2, 7, 6, 5, 4, 3, 2 };

        #region NRIC

        /// <summary>
        ///     Check NRIC/UIN is valid or not 
        /// </summary>
        /// <param name="nric"></param>
        /// <returns></returns>
        public static bool IsValidNric(string nric)
        {
            if (string.IsNullOrWhiteSpace(nric))
            {
                return false;
            }

            // Check length

            if (nric.Length != 9)
            {
                return false;
            }

            int total = 0;

            int count = 0;

            char first = nric.First();

            char last = nric.Last();

            if (first != 'S' && first != 'T')
            {
                return false;
            }

            if (!int.TryParse(nric.Substring(1, nric.Length - 2), out var numericNric))
            {
                return false;
            }

            while (numericNric != 0)
            {
                total += numericNric % 10 * Multiples[Multiples.Length - (1 + count++)];

                numericNric /= 10;
            }

            var outputs = first == 'S'
                ? new[] { 'J', 'Z', 'I', 'H', 'G', 'F', 'E', 'D', 'C', 'B', 'A' }
                : new[] { 'G', 'F', 'E', 'D', 'C', 'B', 'A', 'J', 'Z', 'I', 'H' };

            return last == outputs[total % 11];
        }

        public static bool IsValidFin(string fin)
        {
            if (string.IsNullOrWhiteSpace(fin))
            {
                return false;
            }

            // Check length

            if (fin.Length != 9)
            {
                return false;
            }

            int total = 0;

            int count = 0;

            char first = fin.First();

            char last = fin.Last();

            if (first != 'F' && first != 'G')
            {
                return false;
            }

            if (!int.TryParse(fin.Substring(1, fin.Length - 2), out var numericNric))
            {
                return false;
            }

            while (numericNric != 0)
            {
                total += numericNric % 10 * Multiples[Multiples.Length - (1 + count++)];

                numericNric /= 10;
            }

            var outputs = first == 'F'
                ? new[] { 'X', 'W', 'U', 'T', 'R', 'Q', 'P', 'N', 'M', 'L', 'K' }
                : new[] { 'R', 'Q', 'P', 'N', 'M', 'L', 'K', 'X', 'W', 'U', 'T' };

            return last == outputs[total % 11];
        }

        public static bool IsValidNricOrFin(string nricOrFin)
        {
            if (string.IsNullOrWhiteSpace(nricOrFin))
            {
                return false;
            }

            // Check length

            if (nricOrFin.Length != 9)
            {
                return false;
            }

            char first = nricOrFin.First();

            if (first == 'S' || first == 'T')
            {
                return IsValidNric(nricOrFin);
            }

            if (first == 'F' || first == 'G')
            {
                return IsValidFin(nricOrFin);
            }

            return false;
        }

        #endregion

        public static bool IsValidDomain(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                return false;
            }

            // Check html

            if (url.Length <= 8)
            {
                return false;
            }

            //char first = nricOrFin.First();

            //if (first == 'S' || first == 'T')
            //{
            //    return IsValidNric(nricOrFin);
            //}

            //if (first == 'F' || first == 'G')
            //{
            //    return IsValidFin(nricOrFin);
            //}
            var startString = "Http://";
            var endString = "/";
            bool isStartCorrect = url.ToUpper().StartsWith(startString.ToUpper());
            bool isEndCorrect = url.EndsWith(endString);
            if (!isStartCorrect || !isEndCorrect)
            {
                return false;
            }

            return true;
        }

        public static bool ToBoolean(this Enums.BooleanNA value)
        {
            return value == Enums.BooleanNA.Yes;
        }

        public static bool ToBoolean(this Enums.BooleanNotNA value)
        {
            return value == Enums.BooleanNotNA.Yes;
        }

        public static bool IsSingapore(this Enums.Nationality value)
        {
            return value == Enums.Nationality.SingaporeCitizen;
        }

        public static int GetAgeOfPreviousYear(this DateTimeOffset dateTimeOffset)
        {
            int age = 0;
            var endOfPreviousYear = new DateTime(DateTime.Now.Year - 1, 12, 31);
            age = endOfPreviousYear.Year - dateTimeOffset.DateTime.Year;
            if (endOfPreviousYear.DayOfYear < dateTimeOffset.DateTime.DayOfYear)
                age = age - 1;

            return age;
        }
    }
}