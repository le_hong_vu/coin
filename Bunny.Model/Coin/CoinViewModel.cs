﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Bunny.DataTable.Attributes;

namespace Bunny.Model.Coin
{
    public class CoinViewModel
    {
        [Display(Name = "Symbol")]
        [DataTable(Order = 2, DisplayName = "Symbol", IsSearchable = true)]
        public string Symbol { get; set; }
        [DataTableIgnore]
        public string BidPrice { get; set; }
        [DataTableIgnore]
        public string BidQty { get; set; }
        [DataTableIgnore]
        public string AskPrice { get; set; }
        [DataTableIgnore]
        public string AskQty { get; set; }
        [DataTable(Order = 3, IsVisible = false)]
        public bool IsActive { get; set; }
        [DataTable(Order = 4, IsVisible = false)]
        public int MinDisableNotification { get; set; }
        public int Id { get; set; }
    }

    public class CoinHeaderViewModel : CoinViewModel
    {
        [DataTable(Order = 1, IsVisible = false)]
        public int Id { get; set; }
    }

    public class CoinDataTableModel : CoinHeaderViewModel
    {

    }

    public class Candlestick
    {
        public string Symbol { get; set; }
        public List<PriceVolume> PriceVolume { get; set; }
        public Candlestick()
        {
            PriceVolume = new List<PriceVolume>();
        }
    }
    public class PriceVolume
    {
        public decimal Price { get; set; }
        public decimal Volume { get; set; }
        public PriceVolume(decimal price, decimal volume)
        {
            this.Price = price;
            this.Volume = volume;
        }
    }

    public class PriceStatistics24hr
    {
        public string symbol { get; set; }
        public decimal highPrice { get; set; }
        public decimal lowPrice { get; set; }
        public decimal quoteVolume { get; set; }
        public PriceStatistics24hr()
        {

        }
        public PriceStatistics24hr(string symbol, decimal highPrice, decimal lowPrice, decimal quoteVolume)
        {
            this.symbol = symbol;
            this.highPrice = highPrice;
            this.lowPrice = lowPrice;
            this.quoteVolume = quoteVolume;
        }
    }

    public class ResultConvertCandlestick
    {
        public dynamic One { get; set; }
        public dynamic Two { get; set; }
        public dynamic Three { get; set; }
        public dynamic Four { get; set; }
        public dynamic Five { get; set; }
        public dynamic Six { get; set; }
        public dynamic Seven { get; set; }
        public dynamic Eight { get; set; }
        public dynamic Night { get; set; }
        public dynamic Ten { get; set; }
        public dynamic Eleven { get; set; }
        public dynamic Twelve { get; set; }
    }
}