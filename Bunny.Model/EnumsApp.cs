﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Bunny.Model
{
    public class EnumsApp
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public enum IntervalSetting
        {
            [Display(Name = "1m")]
            OneMin = 1,

            [Display(Name = "3m")]
            ThreeMin = 2,

            [Display(Name = "5m")]
            FiveMin = 3,

            [Display(Name = "15m")]
            FifteenMin = 4,

            [Display(Name = "1h")]
            OneHour = 5,

            [Display(Name = "2h")]
            TwoHour = 6,

            [Display(Name = "4h")]
            FourHour = 7,
        }
    }
}