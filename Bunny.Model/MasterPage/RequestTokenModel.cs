﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Bunny.Model.Constans;

namespace Bunny.Model.MasterPage
{
    //[Validator(typeof(RequestTokenModelValidator))]
    public class RequestTokenModel
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public GrantType GrantType { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string RefreshToken { get; set; }
    }
}