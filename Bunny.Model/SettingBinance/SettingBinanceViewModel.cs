﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Bunny.DataTable.Attributes;
using Bunny.Model.Coin;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Bunny.Model.SettingBinance
{
    public class SettingBinanceViewModel
    {
        [Display(Name = "Limit Price")]
        public double LimitPrice { get; set; }
        [Display(Name = "Desire Price Percent")]
        public double DesirePricePercent { get; set; }
        [Display(Name = "Price Z")]
        public double PriceZ { get; set; }

        [Display(Name = "Limit Volume")]
        public double LimitVolume { get; set; }
        [Display(Name = "Desire Volume Percent")]
        public double DesireVolumePercent { get; set; }
        [Display(Name = "Volume Z")]
        public double VolumeZ { get; set; }

        [Display(Name = "Limit Max Volume")]
        public double LimitMaxVolume { get; set; }
        [Display(Name = "Desire Max Volume Percent")]
        public double DesireMaxVolPercent { get; set; }
        [Display(Name = "Max Volume Z")]
        public double MaxVolZ { get; set; }
        public EnumsApp.IntervalSetting Interval { get; set; }


        [Display(Name = "Is Check Price")]
        public bool IsCheckPrice { get; set; }
        [Display(Name = "Is Check Volume")]
        public bool IsCheckVolume { get; set; }
        [Display(Name = "Is Check Max Volume")]
        public bool IsCheckMaxVolume { get; set; }

        [Display(Name = "Index Max Volume")]
        public int IndexMaxVolume { get; set; }

        [Display(Name = "Safe Percent For Buy")]
        public double SafePercentForBuy { get; set; }
        [Display(Name = "Total Column For Buy")]
        public int TotalColumnForBuy { get; set; }

        [Display(Name = "Safe Percent For Sell")]
        public double SafePercentForSell { get; set; }
        [Display(Name = "Total Column For Sell")]
        public int TotalColumnForSell { get; set; }

        [Display(Name = "Bot Token")]
        public string TokenBotTelegram { get; set; }
        [Display(Name = "Room Bot Id")]
        public string RoomIdBotTelegram { get; set; }

        [Display(Name = "Select Coin")]
        public List<CoinViewModel> Coin { get; set; }
    }

    public class SettingBinanceEditModel : SettingBinanceViewModel
    {
        public SettingBinanceEditModel()
        {
            ListIntervalSetting = new List<SelectListItem>();
        }
        public List<SelectListItem> ListIntervalSetting { get; set; }
        public List<SelectListItem> ListCoin { get; set; }
        public int Check { get; set; }
        public int[] ListCoinId { get; set; }
        public int Id { get; set; }
    }
}