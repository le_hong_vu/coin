﻿using System.ComponentModel.DataAnnotations;
using Bunny.DataTable.Attributes;

namespace Bunny.Model.User
{
    public class NotificationCoinViewModel
    {
        [Display(Name = "Name")]
        [DataTable(Order = 2, DisplayName = "Name", IsSearchable = true)]
        public string Name { get; set; }
        [Display(Name = "Symbol")]
        [DataTable(Order = 3, DisplayName = "Symbol", IsSearchable = true)]
        public string Symbol { get; set; }

        [Display(Name = "Current Value Price")]
        [DataTable(Order = 4, DisplayName = "Current Value Price", IsSearchable = true)]
        public decimal CurrentValuePrice { get; set; }
        [Display(Name = "Avg Value Price")]
        [DataTable(Order = 5, DisplayName = "Avg Value Price", IsSearchable = true)]
        public decimal AvgValuePrice { get; set; }
        [Display(Name = "Increase Percent Price")]
        [DataTable(Order = 6, DisplayName = "Increase Percent Price", IsSearchable = true)]
        public decimal IncreasePercentPrice { get; set; }

        [Display(Name = "Current Value Volume")]
        [DataTable(Order = 7, DisplayName = "Current Value Volume", IsSearchable = true)]
        public decimal CurrentValueVolume { get; set; }
        [Display(Name = "Avg Value Volume")]
        [DataTable(Order = 8, DisplayName = "Avg Value Volume", IsSearchable = true)]
        public decimal AvgValueVolume { get; set; }
        [Display(Name = "Increase Percent Volume")]
        [DataTable(Order = 9, DisplayName = "Increase Percent Volume", IsSearchable = true)]
        public decimal IncreasePercentVolume { get; set; }

        [Display(Name = "Current Value Volume Max")]
        [DataTable(Order = 10, DisplayName = "Current Value Volume Max", IsSearchable = true)]
        public decimal CurrentValueVolumeMax { get; set; }
        [Display(Name = "Avg Value Volume Max")]
        [DataTable(Order = 11, DisplayName = "Avg Value Volume Max", IsSearchable = true)]
        public decimal AvgValueVolumeMax { get; set; }
        [Display(Name = "Increase Percent Volume Max")]
        [DataTable(Order = 12, DisplayName = "Increase Percent Volume Max", IsSearchable = true)]
        public decimal IncreasePercentVolumeMax { get; set; }

        [Display(Name = "Highest Price")]
        [DataTable(Order = 13, DisplayName = "Highest Price", IsSearchable = true)]
        public decimal HighestPrice { get; set; }
        [Display(Name = "Lowest Price")]
        [DataTable(Order = 14, DisplayName = "Lowest Price", IsSearchable = true)]
        public decimal LowestPrice { get; set; }
        [Display(Name = "Quote Volume")]
        [DataTable(Order = 15, DisplayName = "Quote Volume", IsSearchable = true)]
        public decimal QuoteVolume { get; set; }
    }
    public class NotificationCoinHeaderViewModel : NotificationCoinViewModel
    {
        [DataTable(Order = 1, IsVisible = false)]
        public int Id { get; set; }
    }
    public class NotificationCoinDataTableModel : NotificationCoinHeaderViewModel
    {

    }
}