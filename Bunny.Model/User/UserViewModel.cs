﻿using Bunny.DataTable.Attributes;
using System;
using System.ComponentModel.DataAnnotations;
using Bunny.Model.Validators.UserValidator;
using FluentValidation.Attributes;
using FluentValidation.Validators;
using Newtonsoft.Json;

namespace Bunny.Model.User
{
    public class UserViewModel
    {
        [Display(Name = "User Name")]
        [DataTable(Order = 2, DisplayName = "User Name", IsSearchable = true)]
        public string UserName { get; set; }
        [DataTableIgnore]
        public string Password { get; set; }
        [Display(Name = "Active")]
        [DataTable(Order = 3, IsSearchable = true, DisplayName = "Active")]
        public bool IsActive { get; set; }
        [Display(Name = "Last Name")]
        [DataTable(Order = 4, IsSearchable = true, DisplayName = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "First Name")]
        [DataTable(Order = 5, IsSearchable = true, DisplayName = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Phone Number")]
        [DataTable(Order = 6, IsSearchable = true, DisplayName = "Phone Number")]
        public string PhoneNumber { get; set; }
        [DataTableIgnore]
        public string Subject { get; set; }
    }

    [Validator(typeof(UserModelValidator.UserLoginViewModelValidator))]
    public class UserLoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public Guid RefreshToken { get; set; }
        public string ClientId { get; set; }
        public string Subject { get; set; }
    }

    public class UserHeaderViewModel : UserViewModel
    {
        [DataTable(Order = 1, IsVisible = false)]
        public int Id { get; set; }
        [Display(Name = "Manage Portal")]
        [DataTable(Order = 7, IsSearchable = true, DisplayName = "Manage Portal")]
        public bool IsAdminPortal { get; set; }
        [DataTableIgnore]
        public bool IsManager { get; set; }
    }

    public class UserDataTableModel : UserHeaderViewModel
    {

    }
}