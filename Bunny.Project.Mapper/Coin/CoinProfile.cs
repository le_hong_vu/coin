﻿using AutoMapper;
using Bunny.AutoMapper;
using Bunny.Data.EF.Entities;
using Bunny.Model.Coin;

namespace Bunny.Project.Mapper.Coin
{
    public class CoinProfile : Profile
    {
        public CoinProfile()
        {
            CreateMap<CoinEntity, CoinViewModel>().IgnoreAllNonExisting();
            CreateMap<CoinViewModel, CoinEntity>().IgnoreAllNonExisting();

            CreateMap<CoinEntity, CoinDataTableModel>().IgnoreAllNonExisting();
        }
    }
}