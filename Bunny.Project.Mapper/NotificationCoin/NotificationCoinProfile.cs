﻿using AutoMapper;
using Bunny.AutoMapper;
using Bunny.Data.EF.Entities;
using Bunny.Model.User;

namespace Bunny.Project.Mapper.NotificationCoin
{
    public class NotificationCoinProfile : Profile
    {
        public NotificationCoinProfile()
        {
            CreateMap<NotificationCoinViewModel, NotificationCoinEntity>().IgnoreAllNonExisting();
            CreateMap<NotificationCoinEntity, NotificationCoinViewModel>().IgnoreAllNonExisting();

            CreateMap<NotificationCoinEntity, NotificationCoinDataTableModel>().IgnoreAllNonExisting();
        }
    }
}