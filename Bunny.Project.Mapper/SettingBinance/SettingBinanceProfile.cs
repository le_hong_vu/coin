﻿using AutoMapper;
using Bunny.AutoMapper;
using Bunny.Data.EF.Entities;
using Bunny.Model.SettingBinance;

namespace Bunny.Project.Mapper.SettingBinance
{
    public class SettingBinanceProfile : Profile
    {
        public SettingBinanceProfile()
        {
            CreateMap<SettingBinanceViewModel, SettingBinanceEntity>().IgnoreAllNonExisting();
            CreateMap<SettingBinanceEntity, SettingBinanceViewModel>().IgnoreAllNonExisting();
            CreateMap<SettingBinanceEditModel, SettingBinanceEntity>().IgnoreAllNonExisting();
            CreateMap<SettingBinanceEntity, SettingBinanceEditModel>().IgnoreAllNonExisting()
                .ForMember(d => d.Coin, o => o.MapFrom(s => s.Coin));
        }
    }
}