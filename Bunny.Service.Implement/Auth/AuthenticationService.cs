﻿using System.Threading;
using System.Threading.Tasks;
using Bunny.Business.Interface.MasterPage;
using Bunny.DependencyInjection.Attributes;
using Bunny.Middle.Auth;
using Bunny.Middle.Auth.Interfaces;
using Bunny.Middle.Helper;
using Bunny.Middle.Model.User;
using Bunny.Middle.User;
using Bunny.Model.Constans;
using Bunny.Model.MasterPage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.UI.Pages.Account.Manage.Internal;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Service.Implement.Auth
{
    [PerRequestDependency(ServiceType = typeof(IAuthenticationService))]
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IMasterPageBusiness _masterPageBusiness;

        public AuthenticationService(IMasterPageBusiness masterPageBusiness)
        {
            _masterPageBusiness = masterPageBusiness;
        }
        public async Task<AccessTokenModel> SignInAsync(HttpContext httpContext, RequestTokenModel model, CancellationToken cancellationToken = default)
        {
            AccessTokenModel accessTokenModel = null;

            if (model.GrantType == GrantType.Password)
            {
                if (await _masterPageBusiness.CheckValidSignIn(model.UserName, model.Password))
                {
                    LoggedInUser.Current = _masterPageBusiness.SignIn(model.UserName, out string refreshToken, null);
                    //var test = _masterPageBusiness.SignIn(model.UserName, out string refreshToken, null);
                    //var m = _masterPageBusiness.SignIn(model.UserName, out string refreshToken, null);
                    // Generate access token
                    accessTokenModel = TokenHelper.GenerateAccessToken(model.ClientId, LoggedInUser.Current.Subject, AuthConfig.AccessTokenExpireIn, refreshToken);

                    httpContext.User = TokenHelper.GetClaimsPrincipal(accessTokenModel.AccessToken);


                }
            }
            else if (model.GrantType == GrantType.RefreshToken)
            {
                _masterPageBusiness.CheckValidRefreshToken(model.RefreshToken, null);

                LoggedInUser.Current = await _masterPageBusiness.GetLoggedInUserByRefreshTokenAsync(model.RefreshToken, cancellationToken).ConfigureAwait(true);

                // Generate access token
                accessTokenModel = TokenHelper.GenerateAccessToken(model.ClientId, LoggedInUser.Current.Subject, AuthConfig.AccessTokenExpireIn, model.RefreshToken);

                httpContext.User = TokenHelper.GetClaimsPrincipal(accessTokenModel.AccessToken);
            }
            return accessTokenModel;
        }

        public async Task SignInCookieAsync(HttpContext httpContext, AccessTokenModel accessTokenModel,
            CancellationToken cancellationToken = default)
        {
            TokenHelper.SetAccessTokenInCookie(httpContext.Response.Cookies, accessTokenModel);

            LoggedInUser.Current = await GetLoggedInUserAsync(accessTokenModel.AccessToken, cancellationToken).ConfigureAwait(true);

            if (LoggedInUser.Current != null && LoggedInUser.Current.RoleId == 1)
            {
                TokenHelper.SetHangfireInCookie(httpContext.Response.Cookies);
            }

            httpContext.User = TokenHelper.GetClaimsPrincipal(accessTokenModel.AccessToken);
        }

        public async Task<AccessTokenModel> SignInCookieAsync(HttpContext httpContext, CancellationToken cancellationToken = default)
        {
            var accessTokenModel = TokenHelper.GetAccessTokenInCookie(httpContext.Request.Cookies);

            if (accessTokenModel == null)
            {
                return null;
            }

            string accessTokenClientId = TokenHelper.GetAccessTokenClientId(accessTokenModel.AccessToken);

            if (!TokenHelper.IsValidToken(accessTokenModel.AccessToken) || !string.IsNullOrWhiteSpace(accessTokenClientId))
            {
                return null;
            }

            LoggedInUser.Current = await GetLoggedInUserAsync(accessTokenModel.AccessToken, cancellationToken).ConfigureAwait(true);

            httpContext.User = TokenHelper.GetClaimsPrincipal(accessTokenModel.AccessToken);

            return accessTokenModel;
        }

        public Task SignOutCookieAsync(HttpContext httpContext, CancellationToken cancellationToken = default)
        {
            TokenHelper.RemoveAccessTokenInCookie(httpContext.Response.Cookies);

            LoggedInUser.Current = null;

            if (httpContext.User != null)
            {
                httpContext.User = null;
            }

            return Task.CompletedTask;
        }

        public Task<LoggedInUserModel> GetLoggedInUserAsync(string accessToken, CancellationToken cancellationToken = default)
        {
            string subject = TokenHelper.GetAccessTokenSubject(accessToken);
            //_userBusiness.CheckExistsBySubject(subject);
            return _masterPageBusiness.GetLoggedInUserBySubjectAsync(subject, cancellationToken);
            //throw new System.NotImplementedException();
        }

        public Task ExpireAllRefreshTokenAsync(string accessToken, CancellationToken cancellationToken = default)
        {
            throw new System.NotImplementedException();
        }

        public Task SendConfirmEmailOrSetPasswordAsync(string email, CancellationToken cancellationToken = default)
        {
            throw new System.NotImplementedException();
        }

        public Task ConfirmEmailAsync(SetPasswordModel model, CancellationToken cancellationToken = default)
        {
            throw new System.NotImplementedException();
        }

        public bool IsExpireOrInvalidConfirmEmailToken(string token)
        {
            throw new System.NotImplementedException();
        }

        public Task SetPasswordAsync(SetPasswordModel model, CancellationToken cancellationToken = default)
        {
            throw new System.NotImplementedException();
        }

        public bool IsExpireOrInvalidSetPasswordToken(string token)
        {
            throw new System.NotImplementedException();
        }

        public bool IsUserAccess(int id)
        {
            throw new System.NotImplementedException();
        }

        public void CheckCurrentPassword(string currentPassword)
        {
            throw new System.NotImplementedException();
        }

        public Task ChangePasswordAsync(ChangePasswordModel model, CancellationToken cancellationToken = default)
        {
            throw new System.NotImplementedException();
        }

        public Task<string> GetUrlBackgroundAsync(CancellationToken cancellationToken = default)
        {
            throw new System.NotImplementedException();
        }

        public Task SendEmailDelegatedAsync(IUrlHelper url, string token, CancellationToken cancellationToken = default)
        {
            throw new System.NotImplementedException();
        }
    }
}