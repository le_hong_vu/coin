﻿using System.Threading;
using System.Threading.Tasks;
using Bunny.Business.Interface.Coin;
using Bunny.DataTable.Models.Request;
using Bunny.DataTable.Models.Response;
using Bunny.DependencyInjection.Attributes;
using Bunny.Model.Coin;
using Bunny.Service.Interface.Coin;
using Bunny.Service.Interface.CoinManager;

namespace Bunny.Service.Implement.Coin
{
    [PerRequestDependency(ServiceType = typeof(ICoinService))]
    public class CoinService : ICoinService
    {
        private readonly ICoinBusiness _coinBusiness;

        public CoinService(ICoinBusiness coinBusiness)
        {
            _coinBusiness = coinBusiness;
        }
        public Task<DataTableResponseDataModel<CoinDataTableModel>> GetDataTableAsync(DataTableParamModel model, CancellationToken cancellationToken = default)
        {
            return _coinBusiness.GetDataTableAsync(model, cancellationToken);
        }

        public Task Active(int id, CancellationToken cancellationToken = default)
        {
            return _coinBusiness.Active(id, cancellationToken);
        }

        public Task SetDefaultDisableTimeCoin()
        {
            return _coinBusiness.SetDefaultDisableTimeCoin();
        }

        public Task ChangeMinDisable(int id, int numberChange)
        {
            return _coinBusiness.ChangeMinDisable(id, numberChange);
        }

    }
}