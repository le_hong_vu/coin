﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Bunny.Business.Interface.CoinManager;
using Bunny.DataTable.Models.Request;
using Bunny.DataTable.Models.Response;
using Bunny.DependencyInjection.Attributes;
using Bunny.Model.Binance;
using Bunny.Model.Coin;
using Bunny.Model.User;
using Bunny.Service.Interface.CoinManager;

namespace Bunny.Service.Implement.CoinManager
{
    [PerRequestDependency(ServiceType = typeof(ICoinManagerService))]
    public class CoinManagerService : ICoinManagerService
    {
        private readonly ICoinManagerBusiness _coinManagerBusiness;

        public CoinManagerService(ICoinManagerBusiness coinManagerBusiness)
        {
            _coinManagerBusiness = coinManagerBusiness;
        }
        public Task<DataTableResponseDataModel<NotificationCoinDataTableModel>> GetDataTableAsync(DataTableParamModel model
            , CancellationToken cancellationToken = default)
        {
            return _coinManagerBusiness.GetDataTableAsync(model, cancellationToken);
        }

        public async Task<List<CoinViewModel>> GetListSymbolAsync()
        {
            return await _coinManagerBusiness.GetListSymbolAsync().ConfigureAwait(false);
        }

        public Task SendValueNotificationAsync()
        {
            return _coinManagerBusiness.SendValueNotificationAsync();
        }

        public Task StartJob(int settingId)
        {
            return _coinManagerBusiness.StartJob(settingId);
        }


        public Task WriteLogTest()
        {
            return _coinManagerBusiness.WriteLogTest();
        }
    }
}