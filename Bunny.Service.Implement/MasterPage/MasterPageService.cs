﻿using System.Threading;
using System.Threading.Tasks;
using Bunny.Business.Interface.MasterPage;
using Bunny.DependencyInjection.Attributes;
using Bunny.Middle.Auth;
using Bunny.Middle.Helper;
using Bunny.Middle.Model.User;
using Bunny.Middle.User;
using Bunny.Model.MasterPage;
using Bunny.Model.User;
using Bunny.Service.Interface.MasterPage;
using Microsoft.AspNetCore.Http;

namespace Bunny.Service.Implement.MasterPage
{
    [PerRequestDependency(ServiceType = typeof(IMasterPageService))]
    public class MasterPageService : IMasterPageService
    {
        private readonly IMasterPageBusiness _masterPageBusiness;

        public MasterPageService(IMasterPageBusiness masterPageBusiness)
        {
            _masterPageBusiness = masterPageBusiness;
        }

        public async Task<AccessTokenModel> SignInAsync(HttpContext httpContext, UserLoginModel model, CancellationToken cancellationToken = default)
        {
            AccessTokenModel accessTokenModel = null;
            if (await _masterPageBusiness.CheckValidSignIn(model.UserName, model.Password))
            {
                LoggedInUser.Current = _masterPageBusiness.SignIn(model.UserName, out string refreshToken, null);
                //var m = _masterPageBusiness.SignIn(model.UserName, out string refreshToken, null);
                // Generate access token
                accessTokenModel = TokenHelper.GenerateAccessToken(model.ClientId, "", AuthConfig.AccessTokenExpireIn, refreshToken);

                httpContext.User = TokenHelper.GetClaimsPrincipal(accessTokenModel.AccessToken);
            }

            return accessTokenModel;
        }
    }
}