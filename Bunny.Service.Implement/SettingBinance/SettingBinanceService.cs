﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bunny.Business.Interface.SettingBinance;
using Bunny.Data.EF.Entities;
using Bunny.DependencyInjection.Attributes;
using Bunny.Model.SettingBinance;
using Bunny.Service.Interface.SettingBinance;

namespace Bunny.Service.Implement.SettingBinance
{
    [PerRequestDependency(ServiceType = typeof(ISettingBinanceService))]
    public class SettingBinanceService : ISettingBinanceService
    {
        private readonly ISettingBinanceBusiness _settingBinanceBusiness;

        public SettingBinanceService(ISettingBinanceBusiness settingBinanceBusiness)
        {
            _settingBinanceBusiness = settingBinanceBusiness;
        }
        public async Task<List<SettingBinanceEditModel>> GetSettingBinance()
        {
            return await _settingBinanceBusiness.GetSettingBinance().ConfigureAwait(false);
        }

        public async Task<SettingBinanceEditModel> EditSettingBinance(SettingBinanceEditModel model, int userCurrentId)
        {
            return await _settingBinanceBusiness.EditSettingBinance(model, userCurrentId).ConfigureAwait(false);
        }

        public Task<List<SettingBinanceEntity>> GetAllSettingBinanceEntity()
        {
            return _settingBinanceBusiness.GetAllSettingBinanceEntity();
        }
    }
}