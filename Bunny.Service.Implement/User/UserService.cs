﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Bunny.Business.Interface.User;
using Bunny.DataTable.Models.Request;
using Bunny.DataTable.Models.Response;
using Bunny.DependencyInjection.Attributes;
using Bunny.Model.User;
using Bunny.Service.Interface.User;

namespace Bunny.Service.Implement.User
{
    [PerRequestDependency(ServiceType = typeof(IUserService))]
    public class UserService : IUserService
    {
        private readonly IUserBusiness _userBusiness;

        public UserService(IUserBusiness userBusiness)
        {
            _userBusiness = userBusiness;
        }

        public Task<DataTableResponseDataModel<UserDataTableModel>> GetDataTableAsync(DataTableParamModel model, CancellationToken cancellationToken = default)
        {
            return _userBusiness.GetDataTableAsync(model, cancellationToken);
        }

        public Task<List<UserViewModel>> GetAllUser()
        {
            return _userBusiness.GetAllUser();
        }

        public Task<UserEditModel> GetEdit(int id, CancellationToken cancellationToken = default)
        {
            return _userBusiness.GetEdit(id, cancellationToken);
        }

        public Task<UserAddModel> GetAdd(CancellationToken cancellationToken = default)
        {
            return _userBusiness.GetAdd(cancellationToken);
        }

        public Task<int> SubmitAdd(UserAddModel model, int userId, CancellationToken cancellationToken = default)
        {
            return _userBusiness.SubmitAdd(model, userId, cancellationToken);
        }

        public Task RemoveAsync(int id, int userId, CancellationToken cancellationToken = default)
        {
            return _userBusiness.RemoveAsync(id, userId, cancellationToken);
        }

        public Task<UserEditModel> SubmitEdit(UserEditModel model, int userId, string passwordDefault, CancellationToken cancellationToken = default)
        {
            return _userBusiness.SubmitEdit(model, userId, passwordDefault, cancellationToken);
        }
    }
}