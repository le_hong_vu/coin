﻿using System.Threading;
using System.Threading.Tasks;
using Bunny.DataTable.Models.Request;
using Bunny.DataTable.Models.Response;
using Bunny.Model.Coin;
using Bunny.Model.User;

namespace Bunny.Service.Interface.Coin
{
    public interface ICoinService
    {
        Task<DataTableResponseDataModel<CoinDataTableModel>> GetDataTableAsync(DataTableParamModel model
            , CancellationToken cancellationToken = default);
        Task Active(int id, CancellationToken cancellationToken = default);
        Task SetDefaultDisableTimeCoin();
        Task ChangeMinDisable(int id, int numberChange);
    }
}