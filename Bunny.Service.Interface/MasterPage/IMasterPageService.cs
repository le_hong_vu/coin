﻿using System.Threading;
using System.Threading.Tasks;
using Bunny.Middle.Model.User;
using Bunny.Model.MasterPage;
using Bunny.Model.User;
using Microsoft.AspNetCore.Http;

namespace Bunny.Service.Interface.MasterPage
{
    public interface IMasterPageService
    {
        Task<AccessTokenModel> SignInAsync(HttpContext httpContext, UserLoginModel model, CancellationToken cancellationToken = default);
    }
}