﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bunny.Data.EF.Entities;
using Bunny.Model.SettingBinance;

namespace Bunny.Service.Interface.SettingBinance
{
    public interface ISettingBinanceService
    {
        Task<List<SettingBinanceEditModel>> GetSettingBinance();
        Task<SettingBinanceEditModel> EditSettingBinance(SettingBinanceEditModel model, int userCurrentId);
        Task<List<SettingBinanceEntity>> GetAllSettingBinanceEntity();
    }
}