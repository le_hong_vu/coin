﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.Controllers;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Bunny.Swagger.Filters
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class HideInDocsAttribute : Attribute
    {
    }
    public class HideInDocsFilter : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            foreach (var apiDescriptionGroup in context.ApiDescriptionsGroups.Items)
            foreach (var apiDescription in apiDescriptionGroup.Items)
            {
                var controllerActionDescriptor = apiDescription.ActionDescriptor as ControllerActionDescriptor;

                if (controllerActionDescriptor == null) continue;

                var isHideInDocAttributeInType = controllerActionDescriptor.ControllerTypeInfo
                    .GetCustomAttributes<HideInDocsAttribute>(true)
                    .Any();

                var isHideInDocAttributeInMethod = controllerActionDescriptor.MethodInfo
                    .GetCustomAttributes<HideInDocsAttribute>(true)
                    .Any();

                if (!isHideInDocAttributeInType && !isHideInDocAttributeInMethod) continue;

                var route = "/" + controllerActionDescriptor.AttributeRouteInfo.Template.TrimEnd('/');

                swaggerDoc.Paths.Remove(route);
            }
        }
    }
}