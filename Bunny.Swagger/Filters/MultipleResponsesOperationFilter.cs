﻿using System.Linq;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Bunny.Swagger.Filters
{
    /// <summary>
    ///     Enable multiple SwaggerResponseAttribute with same
    ///     <see cref="Microsoft.AspNetCore.Http.StatusCodes" /> and Support to create and use custom
    ///     attribute inheritance <see cref="SwaggerResponseAttribute" />
    /// </summary>
    public class MultipleResponsesOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var responseAttrs =
                context
                    .ApiDescription
                    .ActionDescriptor
                    .FilterDescriptors
                    .Where(x => x.Filter is SwaggerResponseAttribute)
                    .Select(x => x.Filter as SwaggerResponseAttribute)
                    .GroupBy(x => x.StatusCode);

            foreach (var grouping in responseAttrs)
            {
                var existResponse = operation.Responses.First(x => x.Key == grouping.Key.ToString());
                operation.Responses.Remove(existResponse);

                foreach (var responseAttribute in grouping)
                {
                    var response = new Response
                    {
                        Description = responseAttribute.Description,
                        Examples = existResponse.Value.Examples,
                        Headers = existResponse.Value.Headers,
                        Schema = existResponse.Value.Schema
                    };

                    var key = grouping.Key.ToString();

                    var index = 2;

                    while (operation.Responses.ContainsKey(key))
                    {
                        key = $"{grouping.Key} ({index})";
                        index++;
                    }

                    operation.Responses.Add(key, response);
                }
            }
        }
    }
}