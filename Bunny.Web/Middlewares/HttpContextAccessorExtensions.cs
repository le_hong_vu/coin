﻿

using Bunny.Core.ServiceCollectionUtils;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace System.Web
{
    public static class HttpContext
    {
        private static IHttpContextAccessor _contextAccessor;

        public static Microsoft.AspNetCore.Http.HttpContext Current => _contextAccessor?.HttpContext;

        internal static void Configure(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }
    }
}

namespace Bunny.Web.Middlewares
{
    public static class HttpContextAccessorExtensions
    {
        public static IServiceCollection AddHttpContextAccessorCore(this IServiceCollection services)
        {
            services.AddSingletonIfNotExist<IHttpContextAccessor, HttpContextAccessor>();
            return services;
        }

        /// <summary>
        ///     [Response] Information about executed time 
        /// </summary>
        /// <param name="app"></param>
        public static IApplicationBuilder UseHttpContextAccessor(this IApplicationBuilder app)
        {
            var httpContextAccessor = app.ApplicationServices.GetService<IHttpContextAccessor>();

            System.Web.HttpContext.Configure(httpContextAccessor);

            return app;
        }
    }
}