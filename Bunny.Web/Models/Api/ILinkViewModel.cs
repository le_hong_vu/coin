﻿using Newtonsoft.Json;

namespace Bunny.Web.Models.Api
{
    public interface ILinkViewModel
    {
        string Href { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        string Method { get; set; }
    }
}