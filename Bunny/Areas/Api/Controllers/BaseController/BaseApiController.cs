﻿using Bunny.Core.Constants;
using Bunny.Middle.Auth.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Areas.Api.Controllers.BaseController
{
    [Produces(ContentType.Json, ContentType.Xml)]
    [ServiceFilter(typeof(LoggedInUserBinderFilter))]
    [ServiceFilter(typeof(ApiAuthActionFilter))]
    public class BaseApiController : Controller
    {
        public const string AreaName = "api";
    }
}