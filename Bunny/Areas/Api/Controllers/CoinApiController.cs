﻿using Bunny.Areas.Api.Controllers.BaseController;
using Bunny.DataTable;
using Bunny.DataTable.Models.Request;
using Bunny.Model.Coin;
using Bunny.Service.Interface.Coin;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Areas.Api.Controllers
{
    [Route(EndPoint)]
    public class CoinApiController : BaseApiController
    {
        public const string EndPoint = AreaName + "/coin-api";
        public const string ListingEndpoint = "";

        private readonly ICoinService _coinService;

        public CoinApiController(ICoinService coinService)
        {
            _coinService = coinService;
        }
        /// <summary>
        ///     Get data table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route(ListingEndpoint)]
        [HttpPost]
        public DataTableActionResult<CoinDataTableModel> GetDataTable([FromForm] DataTableParamModel model)
        {
            var result = _coinService.GetDataTableAsync(model);

            var response = result.Result.GetDataTableActionResult();

            return response;
        }
    }
}