﻿using System.Threading.Tasks;
using Bunny.Areas.Api.Controllers.BaseController;
using Bunny.DataTable;
using Bunny.DataTable.Models.Request;
using Bunny.Model.User;
using Bunny.Service.Interface.CoinManager;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Areas.Api.Controllers
{
    [Route(EndPoint)]
    public class CoinManagerApiController : BaseApiController
    {
        public const string EndPoint = AreaName + "/coin-manager-api";
        public const string ListingEndpoint = "";
        public const string GetSymbolsEndpoint = "get-symbols";

        private readonly ICoinManagerService _coinManagerService;

        public CoinManagerApiController(ICoinManagerService coinManagerService)
        {
            _coinManagerService = coinManagerService;
        }
        /// <summary>
        ///     Get data table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route(ListingEndpoint)]
        [HttpPost]
        public DataTableActionResult<NotificationCoinDataTableModel> GetDataTable([FromForm] DataTableParamModel model)
        {
            var result = _coinManagerService.GetDataTableAsync(model);

            var response = result.Result.GetDataTableActionResult();

            return response;
        }

        /// <summary>
        ///     Get symbols
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route(GetSymbolsEndpoint)]
        [HttpGet]
        public async Task<IActionResult> GetSymbols()
        {
            var result = await _coinManagerService.GetListSymbolAsync().ConfigureAwait(false);

            return Json(result);
        }
    }
}