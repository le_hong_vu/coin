﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using Bunny.Areas.Api.Controllers.BaseController;
using Bunny.Service.Interface.Coin;
using Bunny.Service.Interface.CoinManager;
using Bunny.Service.Interface.Test;
using Bunny.TradeCoinFuntion;
using Hangfire;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot;
using static Bunny.TradeCoinFuntion.BinanceClient;
using static Bunny.TradeCoinFuntion.BinanceModel;

namespace Bunny.Areas.Api.Controllers
{
    [Route(EndPoint)]
    public class TestApiController : BaseApiController
    {
        public const string EndPoint = AreaName + "/test";
        public const string TestCreateUserEndpoint = "test-create-user";
        public const string GetAllUserEndpoint = "get-all-user";
        public const string SendNotificationTelegramEndpoint = "send-notification-telegram";
        public const string SendNotificationSymbolEndpoint = "send-notification-symbol";
        public const string TestReadCommandEndpoint = "test-read-cmd";
        public const string SetDisableTimeForCoinEndPoint = "set-disable-time";

        public const string TestWriteLogEndPoint = "Test-Write-Log";

        private readonly ITestService _testService;
        private readonly ICoinManagerService _coinManagerService;
        private readonly ICoinService _coinService;

        public TestApiController(ITestService testService, ICoinManagerService coinManagerService, ICoinService coinService)
        {
            _testService = testService;
            _coinManagerService = coinManagerService;
            _coinService = coinService;
        }
        /// <summary>
        ///     Using for test create user entity
        /// </summary>
        /// <returns></returns>
        [Route(TestCreateUserEndpoint)]
        [HttpGet]
        public async Task<IActionResult> TestCreateUser()
        {
            var result = await _testService.TestAddEntity().ConfigureAwait(false);
            return Json(result);
        }


        /// <summary>
        ///     Test Write Log
        /// </summary>
        /// <returns></returns>
        [Route(TestWriteLogEndPoint)]
        [HttpGet]
        public async Task<IActionResult> TestWriteLog()
        {

            //
            //var x = Crypto.CreateHmac('sha256', key).update('TEST STRING').digest('hex')
            //await _coinManagerService.WriteLogTest().ConfigureAwait(false);
            //
            //var apiKey = "BE81K4WtECn5gAwnpJsXIwXGFswb92RNLqkNzBdfizF53sQKDFnBNg0E1aduDvGN";
            //var timeStamp = DateTime.Now.ToFileTime() + "";
            //var query = "symbol=VarSymbol&side=BUY&type=LIMIT&timeInForce=GTC&quantity=1&price=0.1&recvWindow=5000&timestamp=VarTimeStamp";
            //query.Replace("VarSymbol", "LTCBTC");
            //query.Replace("VarTimeStamp", timeStamp);
            //var x = CreateSignature("query", apiKey);

            var binanceClient = new BinanceClient();
            var binanceService = new BinanceService(binanceClient);

            //GET TICKER DEPTH
            //var getDepth = binanceService.GetDepthAsync("BTCUSDT");
            //Task.WaitAll(getDepth);
            //var depth = getDepth.Result;

            //GET ACCOUNT INFORMATION
            //var getAccount = binanceService.GetAccountAsync();
            //Task.WaitAll(getAccount);
            //var account = getAccount.Result;

            //GET ORDERS FOR SYMBOL
            //var getOrders = binanceService.GetOrdersAsync("BTCUSDT", 100);
            //Task.WaitAll(getOrders);
            //var orders = getOrders.Result;

            //TEST ORDER---------------------------------------------------------------------------
            //var placeOrderTest = binanceService.PlaceTestOrderAsync("NEOBTC", "SELL", 1.00, 00.006151);
            //Task.WaitAll(placeOrderTest);
            //dynamic testOrderResult = placeOrderTest.Result;

            //GET MY TRADES
            //var getMyTrades = binanceService.GetTradesAsync("WTCBTC");
            //Task.WaitAll(getAccount);
            //dynamic trades = getMyTrades.Result;
            //Console.WriteLine(trades);

            //GET ALL PRICES
            //List<Prices> prices = new List<Prices>();
            //prices = binanceService.ListPrices();
            //Console.WriteLine(prices);

            //GET PRICE OF SYMBOL
            double symbol = binanceService.GetPriceOfSymbol("BTCUSDT");
            //Console.WriteLine("Price of BNB: " + symbol);

            //PLACE BUY ORDER
            //var quantity = Math.Round(30 / symbol, 6); ;
            //var placeBuyOrder = binanceService.PlaceBuyOrderAsync("BTCUSDT", quantity, 45000, "LIMIT");
            //Task.WaitAll(placeBuyOrder);
            //dynamic buyOrderResult = placeBuyOrder.Result;
            //Console.WriteLine(buyOrderResult);

            //PLACE SELL ORDER
            //var placeSellOrder = binanceService.PlaceSellOrderAsync("NEOBTC", 1.00, 00.008851);
            //Task.WaitAll(placeSellOrder);
            //dynamic sellOrderResult = placeSellOrder.Result;
            //Console.WriteLine(sellOrderResult);

            //CHECK ORDER STATUS BY ID
            var checkOrder = binanceService.CheckOrderStatusAsync("BTCUSDT", 5366668157);
            Task.WaitAll(checkOrder);
            dynamic checkOrderResult = checkOrder.Result;
            //Console.WriteLine(checkOrderResult);

            //DELETE ORDER BY ID
            var deleteOrder = binanceService.CancelOrderAsync("BTCUSDT", 5366668157);
            Task.WaitAll(deleteOrder);
            dynamic deleteOrderResult = deleteOrder.Result;
            //Console.WriteLine(deleteOrderResult);

            return Json(checkOrderResult);
        }

        private static string CreateSignature(string queryString, string secret)
        {

            byte[] keyBytes = Encoding.UTF8.GetBytes(secret);
            byte[] queryStringBytes = Encoding.UTF8.GetBytes(queryString);
            HMACSHA256 hmacsha256 = new HMACSHA256(keyBytes);

            byte[] bytes = hmacsha256.ComputeHash(queryStringBytes);

            return BitConverter.ToString(bytes).Replace("-", "").ToLower();
        }

        /// <summary>
        ///     Get all user entity
        /// </summary>
        /// <returns></returns>
        [Route(GetAllUserEndpoint)]
        [HttpGet]
        public async Task<IActionResult> GetAllUser()
        {
            var result = await _testService.GetAllUserEntity().ConfigureAwait(false);
            return Json(result);
        }

        /// <summary>
        ///     Test send telegram
        /// </summary>
        /// <returns></returns>
        [Route("test-send-telegram")]
        [HttpGet]
        public async Task<IActionResult> TestSendTelegram(string body, string token, string id)
        {
            var bot = new TelegramBotClient(token);
            var s = await bot.SendTextMessageAsync(id, "Vu Le" + "\n" + "Test");
            return Json("Successfully");
        }

        /// <summary>
        ///     Send notification to telegram
        /// </summary>
        /// <returns></returns>
        [Route(SendNotificationTelegramEndpoint)]
        [HttpGet]
        public async Task<IActionResult> SendNotificationToTelegram()
        {
            var bot = new TelegramBotClient("790784211:AAF-ylli3j4ZAWJMyT8BMwCO_V9iY9Rs5pk");
            var s = await bot.SendTextMessageAsync("692898818", "Vu Le Test");
            //RecurringJob.AddOrUpdate(() => _coinManagerService.SendValueNotificationAsync(), Cron.Minutely);
            return Json("Successfully");
        }

        /// <summary>
        ///     Send notification Symbol
        /// </summary>
        /// <returns></returns>
        [Route(SendNotificationSymbolEndpoint)]
        [HttpGet]
        public async Task<IActionResult> SendNotificationSymbol()
        {
            //await _coinManagerService.SendValueNotificationAsync().ConfigureAwait(false);
            return Json("Successfully");
        }

        /// <summary>
        ///     Test read command
        /// </summary>
        /// <returns></returns>
        [Route(TestReadCommandEndpoint)]
        [HttpGet]
        public async Task<IActionResult> TestReadCommand()
        {
            //* Create your Process
            Process process = new Process();
            process.StartInfo.FileName = "cmd.exe";
            process.StartInfo.Arguments = "/c DIR";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            //* Set ONLY ONE handler here.
            process.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);
            //* Start process
            process.Start();
            //* Read one element asynchronously
            process.BeginErrorReadLine();
            //* Read the other one synchronously
            string output = process.StandardOutput.ReadToEnd();
            Console.WriteLine(output);
            process.WaitForExit();



            return Json(output);
        }

        private static void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //* Do your stuff with the output (write to console/log/StringBuilder)
            //result = outLine.Data;
            Console.WriteLine(outLine.Data);
        }

        /// <summary>
        ///     set default disable time for coin
        /// </summary>
        /// <returns></returns>
        [Route(SetDisableTimeForCoinEndPoint)]
        [HttpGet]
        public async Task<IActionResult> SetDisableTimeForCoin()
        {
            await _coinService.SetDefaultDisableTimeCoin().ConfigureAwait(false);
            return Ok();
        }
    }
}