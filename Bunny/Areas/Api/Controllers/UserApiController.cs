﻿using System.Threading.Tasks;
using Bunny.Areas.Api.Controllers.BaseController;
using Bunny.DataTable;
using Bunny.DataTable.Models.Request;
using Bunny.Middle.Configs;
using Bunny.Model.User;
using Bunny.Service.Interface.User;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Areas.Api.Controllers
{
    [Route(EndPoint)]
    public class UserApiController : BaseApiController
    {
        public const string EndPoint = AreaName + "/user-api";
        public const string ListingEndpoint = "";
        public const string GetAllEndpoint = "all-user";

        private readonly IUserService _userService;

        public UserApiController(IUserService userService)
        {
            _userService = userService;
        }
        /// <summary>
        ///     Get data table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route(ListingEndpoint)]
        [HttpPost]
        public DataTableActionResult<UserDataTableModel> GetDataTable([FromForm] DataTableParamModel model)
        {
            var result = _userService.GetDataTableAsync(model);

            var response = result.Result.GetDataTableActionResult();

            return response;
        }
        /// <summary>
        ///     Get all user
        /// </summary>
        /// <returns></returns>
        [Route(ListingEndpoint)]
        [HttpGet]
        public async Task<IActionResult> GetAllUser()
        {
            var result = await _userService.GetAllUser().ConfigureAwait(false);

            return Json(result);
        }
    }
}