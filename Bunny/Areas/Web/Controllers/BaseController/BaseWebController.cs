﻿using Bunny.Middle.Auth.Attributes;
using Bunny.Middle.Auth.Filters;
using Bunny.Swagger.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Areas.Web.Controllers.BaseController
{
    [HideInDocs]
    [Auth]
    [ServiceFilter(typeof(LoggedInUserBinderFilter))]
    [ServiceFilter(typeof(MvcAuthActionFilter))]
    [Area(AreaName)]
    public class BaseWebController : Controller
    {
        public const string AreaName = "web";
    }
}