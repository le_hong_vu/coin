﻿using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Bunny.Areas.Web.Controllers.BaseController;
using Bunny.Service.Interface.Coin;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Areas.Web.Controllers
{
    [Route(EndPoint)]
    public class CoinController : BaseWebController
    {
        public const string EndPoint = AreaName + "/coin";
        public const string CoinEndpoint = "index";
        public const string ActiveEndpoint = "active";
        public const string ChangeMinDisableNotificationEndpoint = "change-min-disable";

        private readonly ICoinService _coinService;

        public CoinController(ICoinService coinService)
        {
            _coinService = coinService;
        }

        // GET
        [Route(CoinEndpoint)]
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [Route(ActiveEndpoint)]
        [HttpPost]
        public async Task<JsonResult> Active(int id)
        {
            await _coinService.Active(id).ConfigureAwait(false);
            return Json("success");
        }

        [Route(ChangeMinDisableNotificationEndpoint)]
        [HttpPost]
        public async Task<JsonResult> ChangeMinDisableNotification(int id, int numberChange)
        {
            await _coinService.ChangeMinDisable(id, numberChange).ConfigureAwait(false);
            return Json("success");
        }
    }
}