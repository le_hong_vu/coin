﻿using Bunny.Areas.Web.Controllers.BaseController;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Areas.Web.Controllers
{
    [Route(EndPoint)]
    public class CoinManagerController : BaseWebController
    {
        public const string EndPoint = AreaName + "/coin-manager";
        public const string CoinManagerEndpoint = "index";


        // GET
        [Route(CoinManagerEndpoint)]
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
    }
}