﻿using Bunny.Areas.Web.Controllers.BaseController;
using Bunny.Middle.User;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Areas.Web.Controllers
{
    [Route(EndPoint)]
    public class DashBoardController : BaseWebController
    {
        public const string EndPoint = AreaName + "/dash-board";
        public const string DashBoardEndpoint = "";


        [Route(DashBoardEndpoint)]
        [HttpGet]
        public IActionResult Index()
        {
            var x = LoggedInUser.Current;
            return
            View();
        }
    }
}