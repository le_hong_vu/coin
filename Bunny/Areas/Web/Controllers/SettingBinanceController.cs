﻿using System.Threading.Tasks;
using Bunny.Areas.Web.Controllers.BaseController;
using Bunny.Extensions;
using Bunny.Middle.User;
using Bunny.Model.SettingBinance;
using Bunny.Service.Interface.CoinManager;
using Bunny.Service.Interface.SettingBinance;
using Hangfire;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Areas.Web.Controllers
{
    [Route(EndPoint)]
    public class SettingBinanceController : BaseWebController
    {
        public const string EndPoint = AreaName + "/setting-binance";
        public const string SubmitEditEndpoint = "submit-edit";
        public const string StartBinanceJobEndpoint = "start-binance-job";
        public const string StopBinanceJobEndpoint = "stop-binance-job";

        private readonly ISettingBinanceService _settingBinanceService;
        private readonly ICoinManagerService _coinManagerService;

        public SettingBinanceController(ISettingBinanceService settingBinanceService, ICoinManagerService coinManagerService)
        {
            _settingBinanceService = settingBinanceService;
            _coinManagerService = coinManagerService;
        }
        // GET
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var model = await _settingBinanceService.GetSettingBinance().ConfigureAwait(false);
            return View(model);
        }
        [Route(SubmitEditEndpoint)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SubmitEdit([FromForm]SettingBinanceEditModel model)
        {
            var userCurrent = LoggedInUser.Current;
            if (!ModelState.IsValid)
            {
                this.SetNotify("Edit Fail", "Edit binance setting fail, please try again", NotifyStatus.Error);
            }
            var result = await _settingBinanceService.EditSettingBinance(model, userCurrent.Id).ConfigureAwait(false);
            this.SetNotify("Edit Success", "Binance setting has been updated successful", NotifyStatus.Success);
            return RedirectToAction("Index", "SettingBinance");
        }

        /// <summary>
        ///     Start binance job
        /// </summary>
        /// <returns></returns>
        [Route(StartBinanceJobEndpoint)]
        [HttpGet]
        public async Task<IActionResult> StartBinanceJob()
        {
            var settingBinanceEntity = await _settingBinanceService.GetAllSettingBinanceEntity().ConfigureAwait(false);
            var count = 1;
            foreach (var item in settingBinanceEntity)
            {
                RecurringJob.AddOrUpdate("start-binance-job-" + count, () => _coinManagerService.StartJob(item.Id), Cron.Minutely);
                count++;
            }
            //RecurringJob.AddOrUpdate(StartBinanceJobEndpoint, () => _coinManagerService.SendValueNotificationAsync(), Cron.Minutely);
            return Json("Successfully");
        }
        /// <summary>
        ///     Stop binance job
        /// </summary>
        /// <returns></returns>
        [Route(StopBinanceJobEndpoint)]
        [HttpGet]
        public async Task<IActionResult> StopBinanceJob()
        {
            var settingBinanceEntity = await _settingBinanceService.GetAllSettingBinanceEntity().ConfigureAwait(false);
            var count = 1;
            foreach (var item in settingBinanceEntity)
            {
                RecurringJob.RemoveIfExists("start-binance-job-" + count);
                count++;
            }
            //RecurringJob.RemoveIfExists(StartBinanceJobEndpoint);
            return Json("Successfully");
        }
    }
}