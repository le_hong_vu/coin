﻿using System.Threading.Tasks;
using Bunny.Areas.Web.Controllers.BaseController;
using Bunny.DataTable;
using Bunny.DataTable.Models.Request;
using Bunny.Extensions;
using Bunny.Middle;
using Bunny.Middle.Auth.Attributes;
using Bunny.Middle.User;
using Bunny.Model.User;
using Bunny.Service.Interface.User;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Areas.Web.Controllers
{
    [Route(EndPoint)]
    public class UserController : BaseWebController
    {
        public const string EndPoint = AreaName + "/user";
        public const string UserEndpoint = "index";
        public const string ListingEndpoint = "";
        public const string EditEndpoint = "edit";
        public const string SubmitEditEndpoint = "submit-edit";
        public const string AddEndpoint = "add";
        public const string SubmitAddEndpoint = "submit-add";
        public const string RemoveEndpoint = "remove";
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        // GET
        [Route(UserEndpoint)]
        [HttpGet]
        [Auth(Enums.Role.Admin, Enums.Role.Manager)]
        public IActionResult Index()
        {
            return
            View();
        }
        /// <summary>
        ///     Edit 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route(EditEndpoint)]
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var model = await _userService.GetEdit(id).ConfigureAwait(true);
            return View(model);
        }
        [Route(SubmitEditEndpoint)]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> SubmitEdit([FromBody]UserEditModel model)
        {
            if (!ModelState.IsValid)
            {
                //this.SetNotify("Edit Fail", "Edit user fail, please try again", NotifyStatus.Error);
                return Json("Fail");
            }

            await _userService.SubmitEdit(model, LoggedInUser.Current.Id, Constants.UserManager.PasswordDefault)
                .ConfigureAwait(false);
            //this.SetNotify("Edit Success", "User has been updated successful", NotifyStatus.Success);
            return Json("Success");
        }

        /// <summary>
        ///     Add 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route(AddEndpoint)]
        [HttpGet]
        public async Task<IActionResult> Add()
        {
            var model = await _userService.GetAdd().ConfigureAwait(true);
            return View(model);
        }
        [Route(SubmitAddEndpoint)]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> SubmitAdd([FromForm]UserAddModel model)
        {
            if (!ModelState.IsValid)
            {
                this.SetNotify("Edit Fail", "Edit user fail, please try again", NotifyStatus.Error);
                return RedirectToAction("Add");
            }

            await _userService.SubmitAdd(model, LoggedInUser.Current.Id).ConfigureAwait(false);
            this.SetNotify("Edit Success", "User has been updated successful", NotifyStatus.Success);
            return RedirectToAction("Index");
        }

        [Route(RemoveEndpoint)]
        [HttpPost]
       
        public async Task<JsonResult> Remove(int id)
        {
            await _userService.RemoveAsync(id, LoggedInUser.Current.Id).ConfigureAwait(false);
            return Json(new { });
        }
    }
}