﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Bunny.Core.StringUtils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;

namespace Bunny.Binders
{
    public static class StringModelBinderExtensions
    {
        public static IServiceCollection AddStringModelBinder(this IServiceCollection services)
        {
            services.Configure<MvcOptions>(options =>
            {
                var isProviderAdded = options.ModelBinderProviders.Any(x => x.GetType() == typeof(StringModelBinderProvider));

                if (isProviderAdded) return;

                options.ModelBinderProviders.Insert(0, new StringModelBinderProvider());
            });

            return services;
        }
    }
    public class StringModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            return context.Metadata.UnderlyingOrModelType == typeof(string) ? new StringModelBinder() : null;
        }
    }

    public class StringModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (valueProviderResult == ValueProviderResult.None)
            {
                return Task.CompletedTask;
            }

            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueProviderResult);

            try
            {
                var value = valueProviderResult.FirstValue;

                object model = value.EncodeHtml();

                bindingContext.Result = ModelBindingResult.Success(model);

                return Task.CompletedTask;
            }
            catch (Exception exception)
            {
                bindingContext.ModelState
                    .TryAddModelError(bindingContext.ModelName, exception, bindingContext.ModelMetadata);

                return Task.CompletedTask;
            }
        }
    }
}