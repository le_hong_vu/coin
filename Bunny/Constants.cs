﻿namespace Bunny
{
    public static class Constants
    {
        public static class TempDataKey
        {
            public const string Notify = nameof(Notify);
        }

        public static class ViewDataKey
        {
            public const string Title = nameof(Title);
            public const string Description = nameof(Description);
            public const string ImageUrl = nameof(ImageUrl);
            public const string FacebookType = nameof(FacebookType);
            public const string ApplicationName = nameof(ApplicationName);
            public const string FacebookAdminId = nameof(FacebookAdminId);
            public const string FacebookPages = nameof(FacebookPages);
            public const string AuthorUrl = nameof(AuthorUrl);
            public const string PublisherUrl = nameof(PublisherUrl);
            public const string TitleLogin = nameof(TitleLogin);
            public const string DescriptionLogin = nameof(DescriptionLogin);
        }

        public static class UserManager
        {
            public const string PasswordDefault = "123456789abmn";
        }
    }
}