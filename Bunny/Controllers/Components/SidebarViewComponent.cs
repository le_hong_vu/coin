﻿using System.Threading.Tasks;
using Bunny.Middle.Auth.Filters;
using Bunny.Middle.User;
using Bunny.Model.User;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Controllers.Components
{
    public class SidebarViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.FromResult<IViewComponentResult>(View("Sidebar")).ConfigureAwait(false);
        }
    }
}