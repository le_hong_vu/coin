﻿using System.Security.Principal;
using System.Threading.Tasks;
using Bunny.Areas.Web.Controllers.BaseController;
using Bunny.AutoMapper;
using Bunny.Controllers.BaseController;
using Bunny.Extensions;
using Bunny.Middle.Auth.Interfaces;
using Bunny.Middle.Exceptions;
using Bunny.Middle.User;
using Bunny.Model.Constans;
using Bunny.Model.MasterPage;
using Bunny.Model.User;
using Bunny.Service.Interface.MasterPage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Controllers
{
    //[Route(Endpoint)]
    public class MasterPageController : BaseDefaultController
    {
        //public const string Endpoint = "master-page";
        public const string LoginSubmitEndpoint = "login-submit";
        public const string LogOutEndpoint = "logout";
        //public const string IndextEndpoint = "index";

        private readonly IMasterPageService _masterPageService;
        private readonly IAuthenticationService _authenticationService;

        public MasterPageController(IMasterPageService masterPageService, IAuthenticationService authenticationService)
        {
            _masterPageService = masterPageService;
            _authenticationService = authenticationService;
        }
        // GET
        [AllowAnonymous]
        //[Route(IndextEndpoint)]
        public async Task<IActionResult> Index()
        {
            //await _authenticationService.SignOutCookieAsync(HttpContext).ConfigureAwait(true);
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "DashBoard", new { area = "web" });
            }
            return View();
        }
        [Route(LoginSubmitEndpoint)]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(UserLoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index", "MasterPage");
            }
            RequestTokenModel requestToken = model.MapTo<RequestTokenModel>();
            requestToken.GrantType = GrantType.Password;
            try
            {
                AccessTokenModel accessTokenModel = await _authenticationService.SignInAsync(HttpContext, requestToken).ConfigureAwait(false);
                // Sign In to Cookie, for web only
                await _authenticationService.SignInCookieAsync(HttpContext, accessTokenModel).ConfigureAwait(true);
                //var x = LoggedInUser.Current;
                return RedirectToAction("Index", "DashBoard", new { area = "web" });
            }
            catch (BunnyException ex)
            {
                if (ex.Code == ErrorCode.UserPasswordOrUserNameWrong)
                {
                    this.SetNotify("Sign-in Fail", "Your password or your username is wrong, please try again", NotifyStatus.Error);
                }
                return RedirectToAction("Index", "MasterPage");
            }
            
        }
        [Route(LogOutEndpoint)]
        [HttpGet]
        public async Task<IActionResult> LogOut()
        {
            HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
            await _authenticationService.SignOutCookieAsync(HttpContext).ConfigureAwait(true);

            return RedirectToAction("Index");
        }
    }
}