﻿using System.Linq;
using Bunny.Middle.Constants;
using Bunny.Middle.Exceptions;
using Bunny.Web.Extensions;
using EnumsNET;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Bunny.Extensions
{
    public static class ControllerExtensions
    {
        public static IApplicationBuilder UseMvcApi(this IApplicationBuilder app)
        {
            // Localizer
            var localizationOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>().Value;
            // ADD Cookie
            var cookieProvider = localizationOptions.RequestCultureProviders.OfType<CookieRequestCultureProvider>().First();
            cookieProvider.CookieName = Culture.CookieName;

            app.UseRequestLocalization(localizationOptions);

                // Root Path and GZip
            app.UseStaticFiles();

            //Config Global Route
            app
                .UseMvc(routes =>
                {
                    routes.MapRoute(name: "areaRoute",
                        template: "{area:exists}/{controller=MasterPage}/{action=Index}");
                    routes.MapRoute(
                        name: "default",
                        template: "{controller=MasterPage}/{action=Index}");
                });
            return app;
            
        }

        public static void SetNotify(this Controller controller, string title, string message,
            NotifyStatus status = NotifyStatus.Info)
        {
            controller.TempData.Set(Constants.TempDataKey.Notify, new NotifyResultViewModel(title, message, status));
        }

        public static void SetNotify(this Controller controller, string title, BunnyException itsException, NotifyStatus status = NotifyStatus.Error)
        {
            var message = string.IsNullOrWhiteSpace(itsException.Message) ? itsException.Code.AsString(EnumFormat.Description) : itsException.Message;

            controller.SetNotify(title, message, status);
        }
    }
}