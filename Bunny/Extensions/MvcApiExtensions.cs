﻿using Bunny.Binders;
using Bunny.Core.Constants;
using Bunny.DataTable;
using Bunny.Middle.AppConfigs;
using Bunny.Middle.Auth;
using Bunny.Middle.Configs;
using Bunny.Model.Validators.Extensions;
using Bunny.Swagger;
using Bunny.Web.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Serilog;

namespace Bunny.Extensions
{
    public static class MvcApiExtensions
    {
        public static IServiceCollection AddMvcApi(this IServiceCollection services,
            IConfigurationRoot configurationRoot)
        {
            services
                // log
                // Mvc Services
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                .AddSingleton<IActionContextAccessor, ActionContextAccessor>()

                // Enable Session to use TempData
                .AddSingleton<ITempDataProvider, CookieTempDataProvider>()

                // [DataTable]
                .AddDataTable(configurationRoot)

                // [Binders]
                .AddStringModelBinder()

                .AddMvc(options =>
                {
                    options.RespectBrowserAcceptHeader = false; // false by default
                    options.Filters.Add(new ProducesAttribute(ContentType.Json));
                    options.Filters.Add(new ProducesAttribute(ContentType.Xml));
                })
                .AddXmlDataContractSerializerFormatters()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = Core.Constants.StandardFormat
                        .JsonSerializerSettings.ReferenceLoopHandling;
                    options.SerializerSettings.NullValueHandling =
                        StandardFormat.JsonSerializerSettings.NullValueHandling;
                    options.SerializerSettings.Formatting =
                        StandardFormat.JsonSerializerSettings.Formatting;
                    options.SerializerSettings.ContractResolver =
                        StandardFormat.JsonSerializerSettings.ContractResolver;

                    // Time Zone - Only Can Support Local, UTC, RountripKind or Unspecific
                    options.SerializerSettings.DateTimeZoneHandling =
                        StandardFormat.JsonSerializerSettings.DateTimeZoneHandling;
                    options.SerializerSettings.DateFormatString = SystemConfig.SystemDateTimeFormat;
                })
                // [Validator] Model Validator, Must after "AddMvc"
                .AddModelValidator();

            //Get data app configs
            AppConfigs.DataHashConfig = configurationRoot.GetSection("HashData").Get<DataHashModel>();
            AppConfigs.BinanceConfig = configurationRoot.GetSection("Binance").Get<BinanceModel>();
            AppConfigs.HangFireConfig = configurationRoot.GetSection("HangFire").Get<HangFireModel>();

            return services;
        }

        public static IApplicationBuilder UseMvcExApi(this IApplicationBuilder app,
            IConfigurationRoot configurationRoot)
        {
            app
                //.UseSerilogRequestLogging()
                // [DataTable]
                .UseApiDocument()
                .UseHttpContextAccessor()
                .UseHybridAuth()
                .UseDataTable(configurationRoot)
                .UseMvcApi();
            return app;
        }
    }
}