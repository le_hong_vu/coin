﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Bunny.Data.EF.Extensions;
using Bunny.Data.EF.IRepository.SettingBinance;
using Bunny.DependencyInjection;
using Bunny.Middle;
using Bunny.Middle.Configs;
using Bunny.Service.Interface.CoinManager;
using Bunny.Service.Interface.InitialData;
using Bunny.Service.Interface.SettingBinance;
using Hangfire;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Flurl.Http;
using Hangfire.Common;
using static Bunny.Middle.Configs.SystemConfigServiceExtensions;
using Serilog;
using Serilog.Events;

namespace Bunny
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildConfiguration();

            string basedir = Directory.GetDirectoryRoot(AppDomain.CurrentDomain.BaseDirectory);
            //basedir + 

            //Log.Logger = new LoggerConfiguration()
            //            .MinimumLevel.Warning()
            //            .WriteTo.RollingFile(@"./log{Date}.txt")
            //            .CreateLogger();
            Log.Logger = new LoggerConfiguration()
                       //.MinimumLevel.Warning()
                       .WriteTo.Logger(p => p.Filter
                            .ByIncludingOnly(e => e.Level == LogEventLevel.Error)
                            .WriteTo.RollingFile(Path.Combine(basedir, "./Log-Error-{Date}.txt")))
                       .WriteTo.Logger(p => p.Filter
                            .ByIncludingOnly(e => e.Level == LogEventLevel.Information || e.Level == LogEventLevel.Warning)
                            .WriteTo.RollingFile(Path.Combine(basedir, "./Log-Information-{Date}.txt")))
                            .CreateLogger();
            Log.Information("this is a log test for Information");
            Log.Error("this is a log test for Error");
            Log.Warning("this is a log test for Warning");

            //var configuration = new ConfigurationBuilder()
            //        .AddJsonFile("appsettings.json")
            //        .Build();

            //var logger = new LoggerConfiguration()
            //    .ReadFrom.Configuration(configuration)
            //    .CreateLogger();

            var host = BuildWebHost(args);

            OnApplicationStart(host);
            host.Run();
            //CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost
                .CreateDefaultBuilder(args)
                .UseSerilog()
                .UseStartup<Startup>()
                .ConfigureLogging(logging => logging.SetMinimumLevel(LogLevel.Warning))
                .UseDefaultServiceProvider(options => options.ValidateScopes = false)
                .UseIISIntegration()
                .Build();
        private static void BuildConfiguration()
        {
            // Build System Config at first time for config Root, in Startup will build again with reload update features
            var builder = new ConfigurationBuilder().AddJsonFile(Middle.Constants.Configuration.AppSettingsJsonFileName, true, false);

            IConfiguration configuration = builder.Build();

            SystemConfigurationHelper.BuildSystemConfig(configuration);
        }
        private static void OnApplicationStart(IWebHost host)
        {

            SystemConfig.SystemVersion = SystemUtils.SystemTimeNow;
            var serviceScopeFactory = (IServiceScopeFactory)host.Services.GetService(typeof(IServiceScopeFactory));

            using (var scope = serviceScopeFactory.CreateScope())
            {
                var services = scope.ServiceProvider;
                services.MigrateDatabase();

                var initService = services.GetService<IInitialService>();
                initService.DummyData();

                var coinManagementService = services.GetService<ICoinManagerService>();
                var recurringJobManager = services.GetService<IRecurringJobManager>();
                var settingBinanceRepository = services.GetService<ISettingBinanceService>();
                var settingRepositoryEntity = settingBinanceRepository.GetAllSettingBinanceEntity().Result;
                var count = 1;
                foreach (var item in settingRepositoryEntity)
                {
                    recurringJobManager.AddOrUpdate("start-binance-job-" + count, Job.FromExpression<ICoinManagerService>(m => m.StartJob(item.Id)), Cron.Minutely());
                    count++;
                }

                //KickoffStaticPage();

            }
        }
        //public static void KickoffStaticPage()
        //{
        //    if (!string.IsNullOrWhiteSpace(SystemConfig.SystemDomainUrl))
        //    {
        //        $"{SystemConfig.SystemDomainUrl}/api/kickoff/{SystemConfig.KickoffApiAccessKey}".PostAsync(null).Wait();
        //    }

        //    else
        //    {
        //        BackgroundJob.Schedule(() => KickoffStaticPage(), new TimeSpan(0, 0, 0, 2));
        //    }
        //}
    }
}
