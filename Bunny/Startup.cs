﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bunny.Core.TypeUtils;
using Bunny.Data.EF.Factory;
using Bunny.DataTable;
using Bunny.DependencyInjection;
using Bunny.Extensions;
using Bunny.Hangfire;
using Bunny.Middle.Auth;
using Bunny.Project.Mapper;
using Bunny.Swagger;
using Bunny.Web.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
namespace Bunny
{
    public class Startup
    {

        public static IConfigurationRoot ConfigurationRoot;

        public static IHostingEnvironment Environment;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile(Middle.Constants.Configuration.AppSettingsJsonFileName, optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            ConfigurationRoot = builder.Build();

            Environment = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                // [HttpContext]
                .AddHttpContextAccessorCore()

                // [Database]
                .AddDatabase()

                // [API Document] Swagger
                .AddApiDocument(typeof(Startup).GetAssembly(), ConfigurationRoot)

                //Dependency Injection
                .AddDependencyInjection(nameof(Bunny))

                // [Mapper]
                .AddAutoMapperBunny()

                // [Background Job] Store Job in Memory. Add param
                // SystemConfigs.DatabaseConnectionString to store job in Sql Server
                .AddHangfire(ConfigurationRoot)

                // [Authentication] Json Web Toke + Cookie
                .AddHybridAuth(ConfigurationRoot)

                //.Configure<CookiePolicyOptions>(options =>
                //{
                //    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                //    options.CheckConsentNeeded = context => true;
                //    options.MinimumSameSitePolicy = SameSiteMode.None;
                //})

                //MVC
                .AddMvcApi(ConfigurationRoot);

            var m = services;
            //services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            //app.UseStaticFiles();
            //app.UseCookiePolicy();

            //app.UseMvc(routes =>
            //{
            //    routes.MapRoute(
            //        name: "default",
            //        template: "{controller=Home}/{action=Index}/{id?}");
            //});
            // [Background Job] Hangfire
            app.UseHangfire();



            // [Mvc - API] Static files configuration, routing [Mvc] Static files configuration, routing
            app.UseMvcExApi(ConfigurationRoot);
            // [API Document] Swagger
            //app.UseApiDocument();
            //    // [HttpContext]
            //app.UseHttpContextAccessor();
            //// [Authentication] Json Web Token + Cookie
            //app.UseHybridAuth();
            ////Config MVC
            //app.UseMvcApi();
        }
    }
}
