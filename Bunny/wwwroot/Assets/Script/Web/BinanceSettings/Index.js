﻿var configsBinance = {
    startBinanceBtn: "#start-binance",
    stopBinanceBtn: "#stop-binance",
    url: {
        startBinance: globalUrl.binanceSetting.startJob,
        stopBinance: globalUrl.binanceSetting.stopJob,
    }
}; 
var func = {
    initPage: function () {
        func.initEvents();
    },
    initEvents: function () {
        $(configsBinance.startBinanceBtn).on('click', function () {
            func.startBinanceBtn();
        });
        $(configsBinance.stopBinanceBtn).on('click', function () {
            func.stopBinanceBtn();
        });
    },
    startBinanceBtn: function () {
            $.ajax
            ({
                type: "get",
                    url: configsBinance.url.startBinance,
                    success: function (data) {
                        Bunny.notify("Job", "Start Job Successfully", "success");
                    },
                error: function () {
                    Bunny.notify("Job", "Start Job fail", "error");
                }
            });
    },
    stopBinanceBtn: function () {
        $.ajax
        ({
            type: "get",
                url: configsBinance.url.stopBinance,
            success: function (data) {
                Bunny.notify("Job", "Stop Job Successfully", "success");
            },
            error: function () {
                Bunny.notify("Job", "Stop Job fail", "error");
            }
        });
    }
}
$(document).ready(function () {
    func.initPage();
});
