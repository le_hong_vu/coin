﻿var configsUser = {
    editBtn: "#save-edit",
    elementForm: {
        $form: $("#add-user-form"),
        $firstName: $("#FirstName"),
        $lastName: $("#LastName"),
        $userName: $("#UserName"),
        $password: $("#Password"),
        $phoneNumber: $("#PhoneNumber"),
        $id: $("#Id"),
        $roleId: $("#RoleId")
    }
}; 
var func = {
    initPage: function () {
        func.initEvents();
    },
    initEvents: function () {
        $(configsUser.editBtn).on('click', function () {
            func.editBtn();
        });
    },
    editBtn: function () {
        var formResult = configsUser.elementForm.$form;
        var result = {
            FirstName: configsUser.elementForm.$firstName.val(),
            LastName: configsUser.elementForm.$lastName.val(),
            UserName: configsUser.elementForm.$userName.val(),
            Password: configsUser.elementForm.$password.val(),
            PhoneNumber: configsUser.elementForm.$phoneNumber.val(),
            Id: configsUser.elementForm.$id.val(),
            RoleId: configsUser.elementForm.$roleId.val()[0]
        }
        console.log("result = ", JSON.stringify(result), "---- url = ", formResult.prop('action'));
        if (formResult.valid()) {
            $.ajax
            ({
                type: "post",
                url: formResult.prop('action'),
                data: JSON.stringify(result),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        Bunny.notify("Edit Role Success", "Edit role Successfully", "success");
                        //console.log("edit user: ", data);
                        //var url = '@Url.AbsoluteAction("Edit", "User", new {id = Model.Id})';
                        //window.location.href = url;
                    },
                error: function () {
                    Bunny.notify("Edit Role Fail", "Edit role fail", "error");
                }
            });
        }
    }
}
$(document).ready(function () {
    func.initPage();
});
